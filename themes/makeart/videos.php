<?php
    /**
     * Template Name: סרטונים
     * TL Framework theme template
     * @author TL Interactive
     */

    the_post();

    get_header();
?>

<div id="videos" class="single">

    <div class="container">
        
        <div class="row">
            <h1 class="section-title">
                <span><?=get_the_title();?></span>
            </h1>
        </div>

        <?
            get_template_part("templates/posts/video", "loop");
        ?>
        
    </div>

</div>

<?php
    get_footer();
?>