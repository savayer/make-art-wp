<?php
    /**
     * Template Name: גלרייה
     * TL Framework theme template
     * @author TL Interactive
     */

    the_post();

    get_header();
?>

<div id="gallery" class="single">

    <div class="container">
        
        <div class="row">
            <h1 class="section-title">
                <span><?=get_the_title();?></span>
            </h1>
        </div>

    </div>

    <?
        get_template_part("templates/posts/gallery", "loop");
    ?>

</div>

<?php
    get_footer();
?>