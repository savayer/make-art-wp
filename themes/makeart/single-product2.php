<?php

/**
 * TL Framework theme template
 * @author TL Interactive
 */

the_post();

get_header();
?>
<section id="product">

    <div class="container">

        <div class="row">
            <h1 class="section-title">
                <span><?=get_the_title();?></span>
            </h1>

            <div class="row product-row">

                <div class="col-md-5">

                    <div class="gallery">

                        <div class="block-title">
                            <h3 class="product-name"><?=get_the_title();?></h3>
                            <span class="manufactor">יצרן: LaerArt</span>
                            <span>מקט: 3345</span>
                        </div>

                        <div class="large-image">
                            <img src="assets/img/product-demo.png" alt="" />
                        </div>

                        <div class="row gallery-images">
                            <div class="col-md-4">
                                <div class="image">
                                    <img src="assets/img/product-demo.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="image">
                                    <img src="assets/img/product-demo.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="image">
                                    <img src="assets/img/product-demo.png" alt="" />
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="description">

                        <h6 class="block-title">תיאור המוצר</h6>

                        <div class="content">
                            <?
                                the_content();
                            ?>
                        </div>

                    </div>

                </div>

                <div class="col-md-7">

                    <form action="" method="POST">


                        <div class="image-upload order-step">

                            <h5 class="block-title step" data-step="1">
                                העלה תמונה
                            </h5>

                            <div class="step-box-content">

                                <div class="settings">

                                    <p class="info">
                                        * עם העלאת התמונה אני מאשר שזכויות התמונה שייכות לי
                                    </p>

                                    <div class="inputs">

                                        <div class="stauration-container">
                                            <input type="range" min="1" max="100" value="50" class="slider" />
                                        </div>

                                        <button type="button" class="button simple blue">היפוך סיטורציה</button>
                                        
                                        <div class="file">
                                            <input type="file" name="" />
                                            <span class="button simple blue">צרף קובץ</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="preview">
                                    <img src="assets/img/product-preview.png" alt="" />
                                </div>
                            </div>

                        </div>

                        <div class="custom-text order-step">
                            
                            <h5 class="block-title step" data-step="2">
                                הכנס טקסט
                            </h5>

                            <div class="step-box-content">
                                
                                <label for="your-text">
                                    הטקסט שלך כאן:
                                </label>

                                <input type="text" id="your-text" name="" />

                            </div>

                        </div>
                        
                        <div class="summary">

                            <div class="price">
                                סה"כ <span>129</span> ₪
                            </div>

                            <button type="button" class="button simple blue bold">הוסף לעגלת הקניות</button>
                            
                        </div>

                    </form>

                </div>

            </div>
        </div>

    </div>

</section>
<?php
get_footer();
?>