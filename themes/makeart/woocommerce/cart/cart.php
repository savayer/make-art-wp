<?php

/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if (!defined('ABSPATH')) {
	exit;
}

wc_print_notices();

do_action('woocommerce_before_cart'); ?>

<form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
	<?php do_action('woocommerce_before_cart_table'); ?>
	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<thead>
			<tr>
				<th class="product-name"><?php esc_html_e('Product', 'woocommerce'); ?></th>
				<th class="product-price"><?php esc_html_e('Price', 'woocommerce'); ?></th>
				<th class="product-quantity"><?php esc_html_e('Quantity', 'woocommerce'); ?></th>
				<th class="product-subtotal"><?php esc_html_e('Total', 'woocommerce'); ?></th>
				<th class="product-remove">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php do_action('woocommerce_before_cart_contents'); ?>

			<?php

		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
			$_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
			$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

			if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
				$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
				?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
						<td class="product-name" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
							<div class="product-info">
						<?php

					printf('<a href="%s"><img src="%s" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" /></a>', esc_url($product_permalink), wp_get_attachment_url((int)$cart_item['variation']['images'][0]['preview']));

					?>
							<div class="info">
							<?
						echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s"><b>%s</b></a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key);

						echo '<br />';
						foreach ($cart_item['variation']['images'] as $key => $image) {
							if ($key != 0)
								echo " | ";
							printf('<a href="%s" target="_blank">%s</a>', esc_url(wp_get_attachment_url($image['preview'])), $image['label']);
						}

							// Backorder notification.
						if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
							echo '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>';
						}
						?>
							</div>
						</div>
					</td>

						<td class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
							<?php
						echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
						?>
						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>"><?php
																																																																																															if ($_product->is_sold_individually()) {
																																																																																																$product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
																																																																																															} else {
																																																																																																$product_quantity = woocommerce_quantity_input(array(
																																																																																																	'input_name' => "cart[{$cart_item_key}][qty]",
																																																																																																	'input_value' => $cart_item['quantity'],
																																																																																																	'max_value' => $_product->get_max_purchase_quantity(),
																																																																																																	'min_value' => '0',
																																																																																																	'product_name' => $_product->get_name(),
																																																																																																), $_product, false);
																																																																																															}

																																																																																															echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item);
																																																																																															?></td>

						<td class="product-subtotal" data-title="<?php esc_attr_e('Total', 'woocommerce'); ?>">
							<?php
						echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
						?>
						</td>

						<td class="product-remove">
							<?php
								// @codingStandardsIgnoreLine
						echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
							esc_url(wc_get_cart_remove_url($cart_item_key)),
							__('Remove this item', 'woocommerce'),
							esc_attr($product_id),
							esc_attr($_product->get_sku())
						), $cart_item_key);
						?>
						</td>
					</tr>
					<?php

			}
		}
		?>

			<?php do_action('woocommerce_cart_contents'); ?>
			<tr class="bottom">
				<td colspan="6" class="actions">

					<button type="submit" class="button" name="update_cart" id="update-cart" style="visibility: hidden;" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>

					<?php do_action('woocommerce_cart_actions'); ?>

					<?php wp_nonce_field('woocommerce-cart'); ?>
				</td>
			</tr>
			<?php do_action('woocommerce_after_cart_contents'); ?>
		</tbody>
	</table>
	<?php do_action('woocommerce_after_cart_table'); ?>
		
	<div class="cart-summary">
		<table class="shop_table woocommerce-checkout-review-order-table">
			<tfoot>

				<tr class="cart-subtotal">
					<th><?php _e('Subtotal', 'woocommerce'); ?></th>
					<td><?php wc_cart_totals_subtotal_html(); ?></td>
				</tr>

				<?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
					<tr class="cart-discount coupon-<?php echo esc_attr(sanitize_title($code)); ?>">
						<th><?php wc_cart_totals_coupon_label($coupon); ?></th>
						<td><?php wc_cart_totals_coupon_html($coupon); ?></td>
					</tr>
				<?php endforeach; ?>

				<?php foreach (WC()->cart->get_fees() as $fee) : ?>
					<tr class="fee">
						<th><?php echo esc_html($fee->name); ?></th>
						<td><?php wc_cart_totals_fee_html($fee); ?></td>
					</tr>
				<?php endforeach; ?>

				<?php if (wc_tax_enabled() && !WC()->cart->display_prices_including_tax()) : ?>
					<?php if ('itemized' === get_option('woocommerce_tax_total_display')) : ?>
						<?php foreach (WC()->cart->get_tax_totals() as $code => $tax) : ?>
							<tr class="tax-rate tax-rate-<?php echo sanitize_title($code); ?>">
								<th><?php echo esc_html($tax->label); ?></th>
								<td><?php echo wp_kses_post($tax->formatted_amount); ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr class="tax-total">
							<th><?php echo esc_html(WC()->countries->tax_or_vat()); ?></th>
							<td><?php wc_cart_totals_taxes_total_html(); ?></td>
						</tr>
					<?php endif; ?>
				<?php endif; ?>

				<?php do_action('woocommerce_review_order_before_order_total'); ?>

				<tr class="order-total">
					<th><?php _e('Total', 'woocommerce'); ?></th>
					<td><?php wc_cart_totals_order_total_html(); ?></td>
				</tr>

				<?php do_action('woocommerce_review_order_after_order_total'); ?>

			</tfoot>
		</table>
	</div>

</form>

<div class="cart-collaterals">
	<?php

/**
 * Cart collaterals hook.
 *
 * @hooked woocommerce_cross_sell_display
 * @hooked woocommerce_cart_totals - 10
 */
do_action('woocommerce_cart_collaterals');
?>
</div>

<?php do_action('woocommerce_after_cart'); ?>
