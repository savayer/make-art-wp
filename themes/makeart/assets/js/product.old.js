var Product = (function ($) {

    var that = this;
    var BLACK_WHITE_FILTER = 0;
    var REMOVE_COLOR_FILTER = 1;
    var BRIGHTNESS_FILTER  = 2;
    var INVERT_FILTER = 3;

    this.CANVAS = [];

    this.IMAGES = [];

    this.TEXTS = [];

    this.CLIPS = [];

    this.CORDS = [];

    this.INVERTS = [];

    this.BRIGHTNESS = [];

    this.basePrice = null;

    this.imagePrice = null;

    this.CURRENT_CANVAS = 0;

    this.BACKUPS = [];

    this.ALLOWED_TYPES = ["png", "jpg", "jpeg", "ico"];

    this.MAX_SIZE = 15;

    fabric.Canvas.prototype.customiseControls({
        ml: {
            action: function (e, target) {
                if (target.isType("image"))
                    that.removeImage(that.CURRENT_CANVAS, $(".layer-" + that.CURRENT_CANVAS + "-related .image-upload"));
                else {
                    that.CANVAS[that.CURRENT_CANVAS].remove(that.TEXTS[that.CURRENT_CANVAS]);
                    that.TEXTS[that.CURRENT_CANVAS] = null;
                    $("#your-text-" + that.CURRENT_CANVAS).val("");
                }
            },
            cursor: "pointer"
        }
    });

    fabric.Object.prototype.customiseCornerIcons({
        settings: {
            borderColor: '#95d8d2',
            cornerSize: 20,
            cornerShape: 'rect',
            cornerBackgroundColor: '#95d8d2',
            cornerPadding: 5
        },
        mtr: {
            icon: TL_Config.assets + '/img/icons/rotate.svg'
        },
        tr: {
            icon: TL_Config.assets + '/img/icons/resize-top-right.svg'
        },
        bl: {
            icon: TL_Config.assets + '/img/icons/resize-top-right.svg'
        },
        br: {
            icon: TL_Config.assets + '/img/icons/resize-top-left.svg'
        },
        tl: {
            icon: TL_Config.assets + '/img/icons/resize-top-left.svg'
        },
        ml: {
            icon: TL_Config.assets + '/img/icons/trash.svg'
        }
    }, function () {
        that.CANVAS[that.CURRENT_CANVAS].renderAll();
    });

    $(document).ready(function () {

        that.basePrice = parseFloat($("#price").data("base"));

        that.imagePrice = parseFloat($("#price").data("image"));

        $(document).on('touchstart', function(e) {
            if (e.target.nodeName !== 'INPUT' && $("body").is(".disable-scroll")) {
                e.preventDefault();
            }
        });

        $(document).on('touchmove', function(e) {
            if (e.target.nodeName == 'INPUT' && $("body").is(".disable-scroll")) {
                e.preventDefault();
            }
        });

        $(".stauration-container button").click(function() {
            var 
                container = $(this).parent(),
                value = parseFloat($('input', container).val());

            if($(this).is(".plus"))
                value -= 0.025;
            else
                value += 0.025;

            $('input', container).val(value).trigger("input");
        });

        $(".gamma-input").bind('input', function() {
            that.BRIGHTNESS[that.CURRENT_CANVAS] = $(this).val();
            that.create_final_image();
        });
        $(".invert-button").trigger("input");

        $(".invert-button").click(function() {
            if($(this).is(".on"))
                $(this).removeClass("on");
            else
                $(this).addClass("on");

            that.INVERTS[that.CURRENT_CANVAS] = $(this).is(".on");

            that.create_final_image();
        });

        $("#product-form").submit(async function (e) {

            e.preventDefault();

            $("body").addClass("loading");

            that.saveCurrentCanvas();

            $(this).unbind('submit').submit();
        });

        /**
         * Adding Text
         */
        $(".custom-text textarea").bind('input', function () {

            var
                input = $(this),
                wrapper = input.closest(".custom-text"),
                index = Number(wrapper.data("index"));

            if (typeof that.TEXTS[index] === typeof undefined || that.TEXTS[index] == null) {

                that.TEXTS[index] = new fabric.Text(input.val(), {
                    fontFamily: "Arial",
                    fontSize: 18,
                    clipTo: that.clip,
                    top: that.CORDS[index].top + 10,
                    left: that.CORDS[index].left + 10
                });

                that.TEXTS[index].setControlsVisibility({
                    mt: false,
                    mb: false,
                    mr: false,
                    tl: false,
                    bl: false
                });

                that.TEXTS[index].customiseCornerIcons({
                    settings: {
                        borderColor: '#95d8d2',
                        cornerSize: 15,
                        cornerShape: 'rect',
                        cornerBackgroundColor: '#95d8d2',
                        cornerPadding: 1
                    }
                });

                that.CANVAS[index].add(that.TEXTS[index]);
            }
            else {
                that.TEXTS[index].setText(input.val());
                that.CANVAS[index].renderAll();
            }
        });

        /**
         * File Select
         */
        $(".image-upload input[type=file]").change(function () {

            var
                input = this,
                wrapper = $(input).closest(".image-upload"),
                index = Number(wrapper.data("index")),
                fileType,
                fileSize;

            if (input.files && input.files[0]) {

                fileType = String(input.files[0].name).split(".");
                fileType = fileType[fileType.length - 1];
                if (that.ALLOWED_TYPES.indexOf(fileType) == -1) {
                    alert("סוג הקובץ שבחרת אינו נתמך");
                    that.removeImage(index, wrapper);
                    $(input).replaceWith($(input).val('').clone(true));
                    that.updatePrice();
                    return;
                }

                fileSize = input.files[0].size / 1024 / 1024;
                if (fileSize > that.MAX_SIZE) {
                    alert("גודל הקובץ המקסימלי להעלאה הוא " + that.MAX_SIZE + "MB");
                    that.removeImage(index, wrapper);
                    $(input).replaceWith($(input).val('').clone(true));
                    that.updatePrice();
                    return;
                }

                var reader = new FileReader();

                reader.onload = function (e) {

                    var preview = wrapper.find(".preview > img");

                    wrapper.removeClass("empty");
                    preview.attr("src", e.target.result);

                    if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null)
                        that.CANVAS[index].remove(that.IMAGES[index]);

                    var imgObject = new Image();

                    imgObject.src = e.target.result;

                    imgObject.onload = function () {

                        var
                            img = new fabric.Image(imgObject);

                        img.scale(600 / imgObject.width);

                        img.cloneAsImage(function (img) {

                            img.filters.push(new fabric.Image.filters.BlackWhite());

                            img.applyFilters();
                            
                            img.cloneAsImage(function(img) {

                                that.BACKUPS[index] = img;
                            });

                            img.cloneAsImage(function (img) {

                                img.filters.push(new fabric.Image.filters.RemoveColor({
                                    threshold: 1,
                                    distance: 0.4
                                }));

                                img.applyFilters();

                                img.cloneAsImage(function(img){

                                    img.scale(0.2);

                                    img.set({
                                        angle: 0,
                                        clipTo: that.clip,
                                        top: that.CORDS[index].top + 10,
                                        left: that.CORDS[index].left + 10
                                    });

                                    img.setControlsVisibility({
                                        mt: false,
                                        mb: false,
                                        mr: false
                                    });

                                    that.IMAGES[index] = img;

                                    if ($("#product").data("color") != null)
                                        that.IMAGES[index].filters.push(new fabric.Image.filters.BlendColor({
                                            mode: "tint",
                                            color: $("#product").data("color"),
                                            alpha: 1
                                        }));

                                    that.IMAGES[index].filters.push(new fabric.Image.filters.Noise({
                                        noise: 100
                                    }));

                                    that.IMAGES[index].applyFilters();

                                    that.CANVAS[index].add(that.IMAGES[index]);

                                    that.INVERTS[index] = false;

                                    that.BRIGHTNESS[index] = 0;

                                    that.CANVAS[index].renderAll();
                                });
                            });
                        });
                    };
                };

                reader.readAsDataURL(input.files[0]);
            }
            else
                that.removeImage(index, wrapper);

            that.updatePrice();
        });

        /**
         * Initiate all canvases needed
         */
        $(".canvas-layer").each(function () {

            var
                canvasLayer = $(this),
                canvasWidth = canvasLayer.width(),
                canvasHeight = canvasLayer.height(),
                canvasIndex = Number($(this).data("index")),
                croppedWidth = Number($(this).data("width")),
                croppedHeight = Number($(this).data("height")),
                croppedX = Number($(this).data("x")),
                croppedY = Number($(this).data("y")),
                imageObject,
                image,
                clip,
                canvas;

            $("canvas", $(this)).attr("width", $(this).width()).attr("height", $(this).height());

            canvas = new fabric.Canvas("canvas-layer-" + canvasIndex, {
                allowTouchScrolling: true
            });

            imageObject = new Image();
            imageObject.src = base64Decode($(this).data("image"));
            imageObject.onload = function () {

                /**
                 * Canvas Background
                 */
                image = new fabric.Image(imageObject);

                image.set({
                    selectable: false
                });

                image.scale(canvasLayer.width() / imageObject.width);

                image.setControlsVisibility({
                    bl: false,
                    br: false,
                    mb: false,
                    ml: false,
                    mr: false,
                    mt: false,
                    tl: false,
                    tr: false,
                    mtr: false
                });

                image.lockMovementX = true;
                image.lockMovementY = true;

                canvas.add(image);

                canvas.centerObject(image);

                /**
                 * Canvas Clip
                 */

                that.CORDS[canvasIndex] = {
                    left: Math.round((canvasWidth / 100) * croppedX),
                    top: Math.round((canvasHeight / 100) * croppedY),
                    width: Math.round((canvasWidth / 100) * croppedWidth),
                    height: Math.round((canvasHeight / 100) * croppedHeight)
                };

                clip = new fabric.Rect({
                    originX: 'left',
                    originY: 'top',
                    left: that.CORDS[canvasIndex].left,
                    top: that.CORDS[canvasIndex].top,
                    width: that.CORDS[canvasIndex].width,
                    height: that.CORDS[canvasIndex].height,
                    fill: 'rgba(0,0,0,0.0)',
                    stroke: "rgba(0,0,0,1)",
                    strokeWidth: 2,
                    selectable: false
                });

                clip.set({
                    clipFor: 'canvas'
                });

                that.CLIPS[canvasIndex] = clip;

                canvas.add(that.CLIPS[canvasIndex]);

                canvas.selection = false;

                canvas.renderAll();

                if($(window).width() < 991) {
                    canvas.on("selection:cleared", function() {
                        $("body, html").removeClass("no-scroll");
                    });
    
                    canvas.on("selection:created", function() {
                        $("body, html").addClass("no-scroll");
                    });
                }

                that.CANVAS[canvasIndex] = canvas;
            };
        });

        /**
         * Change between images
         */
        $("#product .gallery .gallery-images .image").click(function () {

            var
                index = $(this).data("index"),
                img = $('img', $(this));

            that.saveCurrentCanvas();

            $("#product").removeClass("layer-" + that.CURRENT_CANVAS);
            $("#product").addClass("layer-" + index);

            $("#product .layer-" + that.CURRENT_CANVAS + "-related").hide();
            $("#product .layer-" + index + "-related").show();

            $("#product .large-image > img").attr("src", img.attr("src"));

            that.CLIPS[index].set({
                stroke: "rgba(0, 0, 0, 1)"
            });

            that.CANVAS[index].renderAll();

            if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null)
                that.CANVAS[index].add(that.IMAGES[index]);

            that.CURRENT_CANVAS = index;
        });
    });

    that.create_final_image = function() {

        that.BACKUPS[that.CURRENT_CANVAS].cloneAsImage(function(img) {

            img.filters[INVERT_FILTER] = new fabric.Image.filters.Invert({
                invert: that.INVERTS[that.CURRENT_CANVAS]
            });

            img.applyFilters();

            img.cloneAsImage(function(img) {
                
                img.filters[BRIGHTNESS_FILTER] = new fabric.Image.filters.Brightness({
                    brightness: that.BRIGHTNESS[that.CURRENT_CANVAS]
                });

                img.applyFilters();

                img.cloneAsImage(function(img) {
                    
                    img.filters[REMOVE_COLOR_FILTER] = new fabric.Image.filters.RemoveColor({
                        threshold: 1,
                        distance: 0.4
                    });

                    if ($("#product").data("color") != null)
                        img.filters.push(new fabric.Image.filters.BlendColor({
                            mode: "tint",
                            color: $("#product").data("color"),
                            alpha: 1
                        }));

                    img.applyFilters();

                    img.scale(0.2);

                    img.set({
                        clipTo: that.clip,
                        width: that.IMAGES[that.CURRENT_CANVAS].width,
                        height: that.IMAGES[that.CURRENT_CANVAS].height,
                        top: that.IMAGES[that.CURRENT_CANVAS].top,
                        left: that.IMAGES[that.CURRENT_CANVAS].left
                    });

                    img.setControlsVisibility({
                        mt: false,
                        mb: false,
                        mr: false
                    });

                    that.CANVAS[that.CURRENT_CANVAS].remove(that.IMAGES[that.CURRENT_CANVAS]);
                    that.IMAGES[that.CURRENT_CANVAS] = img;
                    that.CANVAS[that.CURRENT_CANVAS].add(that.IMAGES[that.CURRENT_CANVAS]);
                    that.CANVAS[that.CURRENT_CANVAS].renderAll();
                });

            });
        });
    };

    /**
     * Saving current canvas data
     */
    that.saveCurrentCanvas = function () {

        that.CLIPS[that.CURRENT_CANVAS].set({
            stroke: "rgba(0, 0, 0, 0)"
        });

        that.CANVAS[that.CURRENT_CANVAS].renderAll();

        var
            canvas = document.getElementById("canvas-layer-" + that.CURRENT_CANVAS),
            data = canvas.toDataURL();

        $("#preview-" + that.CURRENT_CANVAS).val(base64Encode(data));

        if (typeof that.IMAGES[that.CURRENT_CANVAS] !== typeof undefined && that.IMAGES[that.CURRENT_CANVAS] !== null)
            that.CANVAS[that.CURRENT_CANVAS].remove(that.IMAGES[that.CURRENT_CANVAS]);
    };

    /**
     * Remove image when something went wrong
     */
    that.removeImage = function (index, wrapper) {

        if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null)
            that.CANVAS[index].remove(that.IMAGES[index]);

        that.IMAGES[index] = null;

        var input = $('input[type=file]', wrapper);
        $(input).replaceWith($(input).val('').clone(true));

        wrapper.addClass("empty");
    };

    // Since the `angle` property of the Image object is stored 
    // in degrees, we'll use this to convert it to radians.
    that.degToRad = function (degrees) {
        return degrees * (Math.PI / 180);
    };

    /**
     * Clipping by clip name
     */
    that.clip = function (ctx) {
        this.setCoords();
        var clipRect = that.CLIPS[that.CURRENT_CANVAS];
        var scaleXTo1 = (1 / this.scaleX);
        var scaleYTo1 = (1 / this.scaleY);
        ctx.save();

        var ctxLeft = -(this.width / 2) + clipRect.strokeWidth;
        var ctxTop = -(this.height / 2) + clipRect.strokeWidth;
        var ctxWidth = clipRect.width - clipRect.strokeWidth;
        var ctxHeight = clipRect.height - clipRect.strokeWidth;

        ctx.translate(ctxLeft, ctxTop);

        ctx.rotate(that.degToRad(this.angle * -1));
        ctx.scale(scaleXTo1, scaleYTo1);
        ctx.beginPath();
        ctx.rect(
            clipRect.left - this.oCoords.tl.x,
            clipRect.top - this.oCoords.tl.y,
            clipRect.width,
            clipRect.height
        );
        ctx.closePath();
        ctx.restore();
    };

    /**
     * Update Price
     */
    that.updatePrice = function () {

        var
            imagesCount = 0,
            finalPrice = that.basePrice;

        $(".image-upload input[type=file]").each(function () {
            var input = this;
            if (input.files && input.files[0])
                imagesCount++;
        });

        if (imagesCount)
            finalPrice += (that.basePrice / 100) * (that.imagePrice * imagesCount);

        $("#price span").text(finalPrice.toFixed(2));
    };

    return this;

})(jQuery);

var base64Decode = function (str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
};

var base64Encode = function (str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
};

var sleep = function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};