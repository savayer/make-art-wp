(function($) {

    var
        CROPPER = null,
        CROP_IMAGES = [],
        IMAGES_TEMPLATE = null,
        CURRENT_IMAGE = null;

    $(document).ready(function() {

        if(!$("#crop_settings_tab_data").length)
            return;

        IMAGES_TEMPLATE = base64Decode($("#crop_settings_tab_data .image-list").data("template"));

        if($("#crop_settings_tab_data").data("saved") != "")
            CROP_IMAGES = JSON.parse(base64Decode($("#crop_settings_tab_data").data("saved")));

        var media = null;

        init_images();
        
        save_data();

        $("#crop_settings_tab_data button.add-image").click(function() {

            if(media == null) {

                media = wp.media.frames.media = wp.media();

                media.on("select", function() {

                    var attachment = media.state().get('selection').first().toJSON();
                    
                    CROP_IMAGES.push({
                        id: attachment.id,
                        url: attachment.url,
                        label: "",
                        crop: {
                            data: null,
                            width: null,
                            height: null
                        }
                    });

                    save_data();

                    init_images();

                    open_image_cropper(CROP_IMAGES.length-1);
                });
            }
                
            media.open();
        });

        $("#crop_settings_tab_data .image-settings .actions button").click(function() {
            if($(this).is(".cancel"))
                $("#crop_settings_tab_data .image-settings").removeClass("open");
            else if($(this).is(".remove")) {
                if(confirm("האם אתה בטוח?")) {
                    CROP_IMAGES.splice(CURRENT_IMAGE, 1);
                    save_data();
                    $("#crop_settings_tab_data .image-settings").removeClass("open");
                    init_images();
                }
            }
            else
                save_image();
        });
    });

    var init_images = function() {

        var container = $("#crop_settings_tab_data .image-list");

        container.html("");

        for(var i in CROP_IMAGES) {

            var html = IMAGES_TEMPLATE;

            html = html.replace("{{attachment.index}}", i);
            html = html.replace("{{attachment.url}}", CROP_IMAGES[i].url);

            container.append(html);
        }

        $("a", container).click(function() {
            open_image_cropper($(this).data("index"));            
        });
    };

    var open_image_cropper = function(index) {

        var container = $("#crop_settings_tab_data .image-cropper-inner");

        CURRENT_IMAGE = index;

        $("#image-label").val(CROP_IMAGES[index].label);
        $("#image-order").html("");

        if(CROPPER != null)
            CROPPER.destroy();

        container.html('<img src="' + CROP_IMAGES[index].url + '" id="image-cropper" alt="" />');

        CROPPER = new Cropper(document.getElementById("image-cropper"), {
            viewMode: 3,
            data: CROP_IMAGES[index].crop.data,
            modal: false,
            guides: false,
            zoomable: false,
            zoomOnTouch: false,
            zoomOnWheel: false
        });

        for(var i in CROP_IMAGES) {

            var position = Number(Number(i) + 1);
            
            if(i == index)
                $("#image-order").append('<option value="' + i + '" selected="selected">מקום ' + position + '</option>');
            else
                $("#image-order").append('<option value="' + i + '">מקום ' + position + '</option>');
        }

        $("#crop_settings_tab_data .image-settings").addClass("open");
    };

    var save_image = function() {
        
        var image = CROP_IMAGES[CURRENT_IMAGE];

        image.label = $("#image-label").val();
        image.crop.data = CROPPER.getData();

        image.crop.width = document.getElementById("image-cropper").naturalWidth;
        image.crop.height = document.getElementById("image-cropper").naturalHeight;

        var newIndex = Number($("#image-order").val());

        if(CURRENT_IMAGE != newIndex)
            CROP_IMAGES[CURRENT_IMAGE] = CROP_IMAGES[newIndex];

        CROP_IMAGES[newIndex] = image;

        save_data();

        $("#crop_settings_tab_data .image-settings").removeClass("open");

        init_images();
    };

    var save_data = function() {
        var json = JSON.stringify(CROP_IMAGES);        
        $("#crop-images-input").val(base64Encode(json));
    };

})(jQuery);

var base64Decode = function(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
};

var base64Encode = function(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
    }));
};