var Product = (function ($) {

    var that = this;
    var BLACK_WHITE_FILTER = 0;
    var REMOVE_COLOR_FILTER = 1;
    var BRIGHTNESS_FILTER = 2;
    var INVERT_FILTER = 3;

    this.CANVAS = [];

    this.IMAGES = [];

    this.THUMBS = [];

    this.TEXTS = [];

    this.CLIPS = [];

    this.CORDS = [];

    this.INVERTS = [];

    this.ICONS = [];

    this.BRIGHTNESS = [];

    this.BACKUPS = [];

    this.TEMP_CANVAS = [];

    this.TEMP_ICON_CANVAS = [];

    this.FONTS = [];

    this.basePrice = null;

    this.imagePrice = null;

    this.CURRENT_CANVAS = 0;

    this.ALLOWED_TYPES = ["png", "jpg", "jpeg", "ico"];

    this.MAX_SIZE = 15;

    this.COLOR = [0, 0, 0];

    fabric.Canvas.prototype.customiseControls({
        ml: {
            action: function (e, target) {
                if (target.isType("image")) {
                    console.log(target);
                    if (target.iconSlot != null) {
                        console.log(true);
                        that.CANVAS[that.CURRENT_CANVAS].remove(that.ICONS[that.CURRENT_CANVAS][target.iconSlot]);
                        that.ICONS[that.CURRENT_CANVAS][target.iconSlot] = null;
                    }
                    else
                        that.removeImage(that.CURRENT_CANVAS, $(".layer-" + that.CURRENT_CANVAS + "-related .image-upload"));
                }
                else {
                    that.CANVAS[that.CURRENT_CANVAS].remove(that.TEXTS[that.CURRENT_CANVAS][target.textSlot]);
                    that.TEXTS[that.CURRENT_CANVAS][target.textSlot] = null;

                    var textSlotContainer = $(".text-" + that.CURRENT_CANVAS + "-" + target.textSlot);

                    if (textSlotContainer.closest(".custom-text").find(".text-slot").length > 1)
                        $(".text-" + that.CURRENT_CANVAS + "-" + target.textSlot).remove();
                    else
                        $(".text-" + that.CURRENT_CANVAS + "-" + target.textSlot + " textarea").val("");
                }
            },
            cursor: "pointer"
        }
    });

    fabric.Object.prototype.customiseCornerIcons({
        settings: {
            borderColor: '#95d8d2',
            cornerSize: 20,
            cornerShape: 'rect',
            cornerBackgroundColor: '#95d8d2',
            cornerPadding: 5
        },
        mtr: {
            icon: TL_Config.assets + '/img/icons/rotate.svg'
        },
        tr: {
            icon: TL_Config.assets + '/img/icons/resize-top-right.svg'
        },
        bl: {
            icon: TL_Config.assets + '/img/icons/resize-top-right.svg'
        },
        br: {
            icon: TL_Config.assets + '/img/icons/resize-top-left.svg'
        },
        tl: {
            icon: TL_Config.assets + '/img/icons/resize-top-left.svg'
        },
        ml: {
            icon: TL_Config.assets + '/img/icons/trash.svg'
        }
    }, function () {
        if (typeof that.CANVAS[that.CURRENT_CANVAS] != typeof undefined)
            that.CANVAS[that.CURRENT_CANVAS].renderAll();
    });

    $(document).ready(function () {

        that.basePrice = parseFloat($("#price").data("base"));
        that.imagePrice = parseFloat($("#price").data("image"));

        if ($("#product").data("color") != null)
            that.COLOR = hexToRgb("#" + $("#product").data("color"));

        $(document).on('touchstart', function (e) {
            if (e.target.nodeName !== 'INPUT' && $("body").is(".disable-scroll")) {
                e.preventDefault();
            }
        });

        $(document).on('touchmove', function (e) {
            if (e.target.nodeName == 'INPUT' && $("body").is(".disable-scroll")) {
                e.preventDefault();
            }
        });

        $(".stauration-container button").click(function () {
            var
                container = $(this).parent(),
                value = parseFloat($('input', container).val());

            if ($(this).is(".plus"))
                value++;
            else
                value--;

            $('input', container).val(value).trigger("input");
        });

        $(".threshold").bind('input', function () {
            $(this).parent().find("input[type=number]").val($(this).val());
            that.manipulate_image_pixels(that.CURRENT_CANVAS);
        });

        $(".stauration-container input[type=number]").bind('input', function () {
            $(this).parent().find(".threshold").val($(this).val()).trigger("input");
        });

        $(".invert-button").click(function () {
            $(this).toggleClass("on");
            that.manipulate_image_pixels(that.CURRENT_CANVAS);
        });

        $("#product-form").submit(async function (e) {

            e.preventDefault();

            $("body").addClass("loading");

            that.saveCurrentCanvas();

            $(this).unbind('submit').submit();
        });

        $(document).on("click", ".open-font-picker", function () {

            var
                fontPicker = $(this).closest(".custom-text").find(".font-picker"),
                textSlotIndex = parseInt($(this).closest(".text-slot").find("textarea").data("index"));

            if (fontPicker.is(".open") && fontPicker.data("active-index") == textSlotIndex)
                fontPicker.removeClass("open");
            else {
                $("li", fontPicker).each(function () {
                    var font = $(this).css("font-family");
                    font = font.replace('"', '');
                    font = font.replace('"', '');
                    if (font == that.FONTS[that.CURRENT_CANVAS][textSlotIndex])
                        $(this).addClass("active");
                    else
                        $(this).removeClass("active");
                });
                fontPicker.data("active-index", textSlotIndex);
                fontPicker.addClass("open");
            }
        });

        $(".font-picker li a").click(function () {
            var
                font = $(this).parent().css("font-family"),
                fontPicker = $(this).closest(".font-picker"),
                currentIndex = parseInt(fontPicker.data("active-index"));

            font = font.replace('"', '');
            font = font.replace('"', '');

            that.FONTS[that.CURRENT_CANVAS][currentIndex] = font;

            $(this).closest(".font-picker").find(".active").removeClass("active");
            $(this).parent().addClass("active");
            that.TEXTS[that.CURRENT_CANVAS][currentIndex].set({
                fontFamily: font
            });
            that.CANVAS[that.CURRENT_CANVAS].renderAll();
        });

        $(".order-step.custom-text button").click(function () {

            var
                container = $(this).closest(".order-step"),
                textSlotsCount = container.find(".text-slot").length,
                lastTextSlot = container.find(".text-slot:nth-child(" + (textSlotsCount + 1) + ")"),
                clonedSlot = lastTextSlot.clone(),
                newSlotNumber = parseInt(lastTextSlot.find("textarea").data("index")) + 1;

            clonedSlot.find("textarea").attr({
                "data-index": newSlotNumber
            }).val("");

            clonedSlot.attr("class", "text-slot text-" + that.CURRENT_CANVAS + "-" + newSlotNumber);

            lastTextSlot.after(clonedSlot);

            that.FONTS[that.CURRENT_CANVAS][newSlotNumber] = "Arial";
        });

        $(".sticker-picker ul li a").click(function () {

            var
                imgObject = new Image();

            imgObject.onload = function () {

                if (that.TEMP_ICON_CANVAS[that.CURRENT_CANVAS] == null)
                    that.TEMP_ICON_CANVAS[that.CURRENT_CANVAS] = document.createElement("canvas");

                that.TEMP_ICON_CANVAS[that.CURRENT_CANVAS].width = $(this).get(0).naturalWidth;
                that.TEMP_ICON_CANVAS[that.CURRENT_CANVAS].height = $(this).get(0).naturalHeight;

                that.manipulate_icon_pixels(that.CURRENT_CANVAS, imgObject);
            };

            imgObject.src = $("img", $(this)).attr("src");
        });

        /**
         * Adding Text
         */
        $(document).on("input", ".custom-text textarea", function () {

            var
                input = $(this),
                index = Number(input.data("index"));

            if (typeof that.TEXTS[that.CURRENT_CANVAS][index] === typeof undefined || that.TEXTS[that.CURRENT_CANVAS][index] == null) {

                that.TEXTS[that.CURRENT_CANVAS][index] = new fabric.Text(input.val(), {
                    fontFamily: that.FONTS[that.CURRENT_CANVAS][index],
                    fontSize: 18,
                    clipTo: that.clip,
                    fill: $("#product").data("color") != null ? ("#" + $("#product").data("color")) : ("#000000"),
                    top: that.CORDS[that.CURRENT_CANVAS].top + 10,
                    left: that.CORDS[that.CURRENT_CANVAS].left + 10,
                    textSlot: index
                });

                that.TEXTS[that.CURRENT_CANVAS][index].setControlsVisibility({
                    mt: false,
                    mb: false,
                    mr: false,
                    tl: false,
                    bl: false
                });

                that.TEXTS[that.CURRENT_CANVAS][index].customiseCornerIcons({
                    settings: {
                        borderColor: '#95d8d2',
                        cornerSize: 15,
                        cornerShape: 'rect',
                        cornerBackgroundColor: '#95d8d2',
                        cornerPadding: 1
                    }
                });

                that.CANVAS[that.CURRENT_CANVAS].add(that.TEXTS[that.CURRENT_CANVAS][index]);
            }
            else {
                that.TEXTS[that.CURRENT_CANVAS][index].setText(input.val());
                that.CANVAS[that.CURRENT_CANVAS].renderAll();
            }
        });

        /**
         * File Select
         */
        $(".image-upload input[type=file]").change(function () {

            var
                input = this,
                wrapper = $(input).closest(".image-upload"),
                index = Number(wrapper.data("index")),
                fileType,
                fileSize;

            if (input.files && input.files[0]) {

                fileType = String(input.files[0].name).split(".");
                fileType = fileType[fileType.length - 1];
                if (that.ALLOWED_TYPES.indexOf(fileType) == -1) {
                    alert("סוג הקובץ שבחרת אינו נתמך");
                    that.removeImage(index, wrapper);
                    $(input).replaceWith($(input).val('').clone(true));
                    that.updatePrice();
                    return;
                }

                fileSize = input.files[0].size / 1024 / 1024;
                if (fileSize > that.MAX_SIZE) {
                    alert("גודל הקובץ המקסימלי להעלאה הוא " + that.MAX_SIZE + "MB");
                    that.removeImage(index, wrapper);
                    $(input).replaceWith($(input).val('').clone(true));
                    that.updatePrice();
                    return;
                }

                var reader = new FileReader();

                reader.onload = function (e) {

                    var preview = wrapper.find(".preview > img");

                    wrapper.removeClass("empty");
                    preview.attr("src", e.target.result);

                    if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null) {
                        that.CANVAS[index].remove(that.IMAGES[index]);
                        that.IMAGES[index] = null;
                    }

                    var imgObject = new Image();

                    imgObject.onload = function () {

                        if (that.TEMP_CANVAS[index] == null)
                            that.TEMP_CANVAS[index] = document.createElement("canvas");

                        that.TEMP_CANVAS[index].width = imgObject.width;
                        that.TEMP_CANVAS[index].height = imgObject.height;

                        that.BACKUPS[index] = imgObject;

                        that.manipulate_image_pixels(index);
                    };

                    imgObject.src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
            else
                that.removeImage(index, wrapper);

            that.updatePrice();
        });
        /**
         * Initiate all canvases needed
         */
        var
            biggestCanvasHeight = 0,
            canvasFinishedLoading = 0;

        $(".canvas-layer").each(function () {

            var
                canvasLayer = $(this),
                domImage = canvasLayer.closest(".large-image").find("> img"),
                domImageWidth = domImage.width(),
                domImageHeight = domImage.height(),
                canvasWidth = canvasLayer.width(),
                canvasHeight = canvasLayer.height(),
                canvasIndex = Number($(this).data("index")),
                croppedWidth = Number($(this).data("width")),
                croppedHeight = Number($(this).data("height")),
                croppedX = Number($(this).data("x")),
                croppedY = Number($(this).data("y")),
                imageObject,
                image,
                clip,
                canvas;

            $("canvas", $(this)).attr("width", $(this).width()).attr("height", $(this).height());

            canvas = new fabric.Canvas("canvas-layer-" + canvasIndex, {
                allowTouchScrolling: true
            });

            imageObject = new Image();
            imageObject.crossOrigin = 'anonymous';
            imageObject.src = base64Decode($(this).data("image"));
            imageObject.onload = function () {

                /**
                 * Canvas Background
                 */
                image = new fabric.Image(imageObject);

                image.set({
                    selectable: false
                });

                image.scale(domImageWidth / imageObject.width);

                image.setControlsVisibility({
                    bl: false,
                    br: false,
                    mb: false,
                    ml: false,
                    mr: false,
                    mt: false,
                    tl: false,
                    tr: false,
                    mtr: false
                });

                image.lockMovementX = true;
                image.lockMovementY = true;

                that.THUMBS[canvasIndex] = image;

                canvas.add(that.THUMBS[canvasIndex]);

                canvas.centerObject(that.THUMBS[canvasIndex]);

                /**
                 * Canvas Clip
                 */

                that.CORDS[canvasIndex] = {
                    left: Math.round(((domImageWidth / 100) * croppedX) + ((canvasWidth - domImageWidth) / 2)),
                    top: Math.round(((domImageHeight / 100) * croppedY) + ((canvasHeight - domImageHeight) / 2)),
                    width: Math.round((domImageWidth / 100) * croppedWidth),
                    height: Math.round((domImageHeight / 100) * croppedHeight)
                };

                if (domImageHeight > biggestCanvasHeight) {
                    biggestCanvasHeight = domImageHeight;
                    $("#product .large-image").height(biggestCanvasHeight);
                }

                clip = new fabric.Rect({
                    originX: 'left',
                    originY: 'top',
                    left: that.CORDS[canvasIndex].left,
                    top: that.CORDS[canvasIndex].top,
                    width: that.CORDS[canvasIndex].width,
                    height: that.CORDS[canvasIndex].height,
                    fill: 'rgba(0,0,0,0.0)',
                    stroke: "rgba(0,0,0,1)",
                    strokeWidth: 2,
                    selectable: false
                });

                clip.set({
                    clipFor: 'canvas'
                });

                that.CLIPS[canvasIndex] = clip;

                canvas.add(that.CLIPS[canvasIndex]);

                canvas.selection = false;

                canvas.renderAll();

                if ($(window).width() < 991) {
                    canvas.on("selection:cleared", function () {
                        $("body, html").removeClass("no-scroll");
                    });

                    canvas.on("selection:created", function () {
                        $("body, html").addClass("no-scroll");
                    });
                }

                that.FONTS[canvasIndex] = ["Arial"];
                that.CANVAS[canvasIndex] = canvas;
                that.ICONS[canvasIndex] = [];
                that.TEXTS[canvasIndex] = [];

                canvasFinishedLoading++;
                if (canvasFinishedLoading == $(".canvas-layer").length)
                    $("#product .large-image").addClass("loaded");
            };
        });

        /**
         * Change between images
         */
        $("#product .gallery .gallery-images .image").click(function () {

            var
                index = $(this).data("index"),
                img = $('img', $(this));

            that.saveCurrentCanvas();

            $("#product").removeClass("layer-" + that.CURRENT_CANVAS);
            $("#product").addClass("layer-" + index);

            $("#product .layer-" + that.CURRENT_CANVAS + "-related").hide();
            $("#product .layer-" + index + "-related").show();

            $("#product .large-image > img").attr("src", img.attr("src"));

            that.CLIPS[index].set({
                stroke: "rgba(0, 0, 0, 1)"
            });

            that.CANVAS[index].renderAll();

            if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null)
                that.CANVAS[index].add(that.IMAGES[index]);

            that.CURRENT_CANVAS = index;

            that.CANVAS[index].renderAll();
        });
    });

    that.manipulate_icon_pixels = function (index, originalImage) {

        var context = that.TEMP_ICON_CANVAS[index].getContext("2d");

        context.clearRect(0, 0, that.TEMP_ICON_CANVAS[index].width, that.TEMP_ICON_CANVAS[index].height);
        context.drawImage(originalImage, 0, 0);

        var
            thres = 255,
            imageData = context.getImageData(0, 0, that.TEMP_ICON_CANVAS[index].width, that.TEMP_ICON_CANVAS[index].height),
            imageDataPixels = imageData.data;

        for (var i = 0; i < imageDataPixels.length; i += 4) {

            var
                r = imageDataPixels[i],
                g = imageDataPixels[i + 1],
                b = imageDataPixels[i + 2];

            if (0.2126 * r + 0.7152 * g + 0.0722 * b >= thres)
                imageDataPixels[i + 3] = 0;
            else {
                imageDataPixels[i] = that.COLOR[0];
                imageDataPixels[i + 1] = that.COLOR[1];
                imageDataPixels[i + 2] = that.COLOR[2];
            }
        }

        imageData.data = imageDataPixels;
        context.putImageData(imageData, 0, 0);

        var imgObject = new Image();
        imgObject.onload = function () {

            var
                img = new fabric.Image(imgObject);

            img.set({
                angle: 0,
                clipTo: that.clip,
                top: that.CORDS[index].top + 10,
                left: that.CORDS[index].left + 10,
                iconSlot: that.ICONS[index].length
            });
            img.setControlsVisibility({
                mt: false,
                mb: false,
                mr: false
            });
            that.ICONS[index].push(img);
            that.CANVAS[index].add(that.ICONS[index][that.ICONS[index].length - 1]);
            that.CANVAS[index].renderAll();
        };

        imgObject.src = that.TEMP_ICON_CANVAS[index].toDataURL();
    };

    that.manipulate_image_pixels = function (index) {

        var context = that.TEMP_CANVAS[index].getContext("2d");

        context.clearRect(0, 0, that.TEMP_CANVAS[index].width, that.TEMP_CANVAS[index].height);
        context.drawImage(that.BACKUPS[index], 0, 0);

        var
            thres = parseInt($(".layer-" + index + "-related .threshold").val()),
            invert = $(".layer-" + index + "-related .invert-button").is(".on"),
            imageData = context.getImageData(0, 0, that.TEMP_CANVAS[index].width, that.TEMP_CANVAS[index].height),
            imageDataPixels = imageData.data;

        for (var i = 0; i < imageDataPixels.length; i += 4) {

            var
                r = imageDataPixels[i],
                g = imageDataPixels[i + 1],
                b = imageDataPixels[i + 2];

            if (invert) {
                if (0.2126 * r + 0.7152 * g + 0.0722 * b >= thres) {
                    imageDataPixels[i] = that.COLOR[0];
                    imageDataPixels[i + 1] = that.COLOR[1];
                    imageDataPixels[i + 2] = that.COLOR[2];
                }
                else
                    imageDataPixels[i + 3] = 0;
            }
            else {
                if (0.2126 * r + 0.7152 * g + 0.0722 * b >= thres)
                    imageDataPixels[i + 3] = 0;
                else {
                    imageDataPixels[i] = that.COLOR[0];
                    imageDataPixels[i + 1] = that.COLOR[1];
                    imageDataPixels[i + 2] = that.COLOR[2];
                }
            }
        }

        imageData.data = imageDataPixels;
        context.putImageData(imageData, 0, 0);

        var imgObject = new Image();
        imgObject.onload = function () {

            if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null)
                that.IMAGES[index].setElement(imgObject);
            else {
                var img = new fabric.Image(imgObject);
                img.scale($('.gallery').width() / 2 / that.TEMP_CANVAS[index].width);
                img.set({
                    angle: 0,
                    clipTo: that.clip,
                    top: that.CORDS[index].top + 10,
                    left: that.CORDS[index].left + 10
                });
                img.setControlsVisibility({
                    mt: false,
                    mb: false,
                    mr: false
                });
                that.IMAGES[index] = img;
                that.CANVAS[index].add(that.IMAGES[index]);
            }

            that.CANVAS[index].renderAll();
        };
        imgObject.src = that.TEMP_CANVAS[index].toDataURL();
    };

    /**
     * Saving current canvas data
     */
    that.saveCurrentCanvas = function () {

        that.CLIPS[that.CURRENT_CANVAS].set({
            stroke: "rgba(0, 0, 0, 0)"
        });

        that.CANVAS[that.CURRENT_CANVAS].discardActiveObject();

        that.CANVAS[that.CURRENT_CANVAS].renderAll();

        var
            canvas = document.getElementById("canvas-layer-" + that.CURRENT_CANVAS),
            data = canvas.toDataURL();

        $("#preview-" + that.CURRENT_CANVAS).val(base64Encode(data));

        if (typeof that.IMAGES[that.CURRENT_CANVAS] !== typeof undefined && that.IMAGES[that.CURRENT_CANVAS] !== null)
            that.CANVAS[that.CURRENT_CANVAS].remove(that.IMAGES[that.CURRENT_CANVAS]);
    };

    /**
     * Remove image when something went wrong
     */
    that.removeImage = function (index, wrapper) {

        if (typeof that.IMAGES[index] !== typeof undefined && that.IMAGES[index] !== null)
            that.CANVAS[index].remove(that.IMAGES[index]);

        that.IMAGES[index] = null;

        var input = $('input[type=file]', wrapper);
        $(input).replaceWith($(input).val('').clone(true));

        wrapper.addClass("empty");
        
        that.updatePrice();
    };

    // Since the `angle` property of the Image object is stored 
    // in degrees, we'll use this to convert it to radians.
    that.degToRad = function (degrees) {
        return degrees * (Math.PI / 180);
    };

    /**
     * Clipping by clip name
     */
    that.clip = function (ctx) {
        this.setCoords();
        var clipRect = that.CLIPS[that.CURRENT_CANVAS];
        var scaleXTo1 = (1 / this.scaleX);
        var scaleYTo1 = (1 / this.scaleY);
        ctx.save();
    
        var ctxLeft = -( this.width / 2 ) + clipRect.strokeWidth;
        var ctxTop = -( this.height / 2 ) + clipRect.strokeWidth;
        var ctxWidth = clipRect.width - clipRect.strokeWidth + 1;
        var ctxHeight = clipRect.height - clipRect.strokeWidth + 1;
    
        ctx.translate( ctxLeft, ctxTop );
    
        ctx.rotate(degToRad(this.angle * -1));
        ctx.scale(scaleXTo1, scaleYTo1);
        ctx.beginPath();

        var objectHeight = this.oCoords.br.y - this.oCoords.tr.y;
        var textFix = 0;
        if(this.text) {
            var textLines = this.text.split(/\r*\n/).length;
            textFix = (objectHeight / (textLines * 9));
        }

        ctx.rect(
            clipRect.left - this.oCoords.tl.x - textFix,
            clipRect.top - this.oCoords.tl.y - textFix,
            ctxWidth,
            ctxHeight
        );
        ctx.closePath();
        ctx.restore();
    };

    /**
     * Update Price
     */
    that.updatePrice = function () {

        var
            imagesCount = 0,
            finalPrice = that.basePrice;

        $(".image-upload input[type=file]").each(function () {
            var input = this;
            if (input.files && input.files[0])
                imagesCount++;
        });

        if (imagesCount)
            finalPrice += (that.basePrice / 100) * (that.imagePrice * imagesCount);

        $("#price span").text(finalPrice.toFixed(2));
    };

    return this;

})(jQuery);

var base64Decode = function (str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
};

var base64Encode = function (str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
};

var hexToRgb = function (hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : [0, 0, 0];
};

var sleep = function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};