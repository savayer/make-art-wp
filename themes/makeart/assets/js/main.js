(function ($) {

    $(document).ready(function () {

        $(document).on("input", ".product-quantity .quantity input", function() {
            $("#update-cart").trigger("click");
        });

        $("a.video").click(function() {
            
            var videoURL = "https://www.youtube.com/embed/" + $(this).data("video-id");
            
            $("#video-wrapper iframe").hide();
            $("#video-wrapper iframe").attr("src", videoURL);
            $("#video-wrapper iframe").load(function() {
                $("#video-wrapper iframe").show();
            });

            $("#video-wrapper").addClass("open");
        });

        $("#video-wrapper").click(function(){
            if(!$("#video-wrapper .video-inner").is(":hover") || $("#video-wrapper .close-video").is(":hover")) {
                $("#video-wrapper iframe").attr("src", "");
                $("#video-wrapper").removeClass("open");
            }
        });

        /**
         * Responsive Navigation
         */
        $("#toggle-nav").click(function() {
            if($("body").is(".nav-open"))
                $("body").removeClass("nav-open");
            else
                $("body").addClass("nav-open");
        });

        $(".mobile-navigation .dropdown a").click(function() {
            var parent = $(this).parent();

            if(parent.is(".open")) {
                parent.removeClass("open");
                $("ul", parent).hide();
            }
            else {
                parent.addClass("open");
                $("ul", parent).show();
            }
        });

        $("#responsiveContainer").click(function() {
            if($("body").is(".nav-open"))
                $("body").removeClass("nav-open");
        });

        if($("body").is(".home")) {

            var current_gallery_page = 0;
            var gallery_next_page = function() {
                current_gallery_page++;
                if(current_gallery_page > Math.ceil($("#gallery .gallery-row > div").length / 6))
                    current_gallery_page = 1;
                var gallery_count = 0;
                $("#gallery .gallery-row > div").each(function() {
                    if(gallery_count < current_gallery_page * 6 && gallery_count >= (current_gallery_page - 1) * 6)
                        $(this).show();
                    else
                        $(this).hide();
                    gallery_count++;
                });
            };
            gallery_next_page();
            setInterval(gallery_next_page, 5000);
        }
    });

})(jQuery);