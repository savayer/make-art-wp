(function($){
    
    var that = this;
    
    that.CURRENT_PAGE = CURRENT_PAGE;
    
    that.LOAD_IS_ACTIVE = false;
    
    that.BACK_BUTTON = false;
    
    that.loadProducts = function() {
      
        if(!that.LOAD_IS_ACTIVE) {
            
            that.LOAD_IS_ACTIVE = true;
            
            $("#loading").show();

            var url = String(window.location).replace(/\?page\=[0-9]+/, "") + "page/" + (that.CURRENT_PAGE + 1);
            
            $.ajax({
                url: url,
                success: function(result) {
                    $(".archive > .container").append(result);
                    $("#loading").hide();
                    that.CURRENT_PAGE++;
                    History.pushState({page: that.CURRENT_PAGE}, $("head title").text(), "?page=" + that.CURRENT_PAGE);
                    that.LOAD_IS_ACTIVE = false;
                    that.BACK_BUTTON = false;
                }
            });
        }
    };
    
    if(!$("#categories").is(".subcategories")) {
        
        $(window).scroll(function() {
            
            var 
                scrollTop = $(window).scrollTop(),
                offset = $(".archive .products-row:last-child .product:last-child").offset(),
                elementHeight = $(".archive .products-row:last-child .product:last-child").height();
                
            $("#inputScroll").val(scrollTop);

            if(scrollTop > offset.top - $(window).height() + elementHeight + 120 && !that.LOAD_IS_ACTIVE && that.CURRENT_PAGE != MAX_PAGE_NUMBER)
                that.loadProducts();
        });
    }
    
})(jQuery);