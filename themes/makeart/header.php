<?php
    /**
     * TL Framework theme template
     * @author TL Interactive
     */
?>
<!DOCTYPE html>
<html dir="rtl">

<head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <?
        wp_head();
    ?>

</head>

<body <? body_class(); ?>>

    <header id="header">
        <div class="container">
            <div class="row d-flex justify-content-between">
                <div class="header-right-side align-self-center">
                    <ul class="row social-links">
                        <li class="youtube">
                            <a href="<?=get_theme_mod("youtube");?>" target="_blank"></a>
                        </li>
                        <li class="facebook">
                            <a href="<?=get_theme_mod("facebook");?>" target="_blank"></a>
                        </li>
                        <li class="instagram">
                            <a href="<?=get_theme_mod("instagram");?>" target="_blank"></a>
                        </li>
                        <li class="whatsapp">
                            <a href="https://wa.me/?text=<?=urlencode(TL_SITE_URL);?>" target="_blank"></a>
                        </li>
                        <li class="g-plus">
                            <a href="<?=get_theme_mod("google-plus");?>" target="_blank"></a>
                        </li>
                    </ul>
                    <div class="row phone">
                        <?
                            $phone = str_split(get_theme_mod("contact-phone"), 3);
                        ?>
                        <a href="tel:<?=get_theme_mod("contact-phone");?>">
                            <span><?=$phone[0];?></span>
                            <span><?=$phone[1];?></span>
                            <span><?=$phone[2] . $phone[3];?></span>
                        </a>
                    </div>
                </div>
                <a href="<?=TL_SITE_URL;?>" class="logo align-self-center">
                    <img src="<?=get_theme_mod("logo");?>" alt="" />
                </a>
            </div>
                
            <a href="javascript:;" id="toggle-nav">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </a>
                
            <a href="<?=esc_url(wc_get_cart_url());?>" id="mobile-cart">
                <span class="count"><?=WC()->cart->get_cart_contents_count();?></span>
                <img src="<?=TL_CURRENT_THEME;?>/assets/img/icons/blue-cart.png" alt="" />
            </a>
        </div>
        <nav class="navigation">

            <ul class="account-links">
                <li class="user">
                    <?
                        if(!is_user_logged_in()) {
                    ?>
                    <a href="<?=esc_url(get_permalink( get_option('woocommerce_myaccount_page_id')));?>">
                        <i></i>
                        הרשם או התחבר
                    </a>
                    <?
                        }
                        else {

                            $user = wp_get_current_user();
                    ?>
                    <a href="<?=esc_url(get_permalink( get_option('woocommerce_myaccount_page_id')));?>">
                        <i></i>
                        <span class="username"><?=$user->display_name;?></span>
                    </a>
                    <?
                        }
                    ?>
                </li>
                <li class="cart">
                    <a href="<?=esc_url(wc_get_cart_url());?>">
                        <i></i>
                        <span class="count"><?=WC()->cart->get_cart_contents_count();?></span>
                    </a>
                </li>
            </ul>

            <div class="container ddd">
                



<ul>




<?php wp_nav_menu('menu=תפריט ראשי'); ?>




                   
                </ul>
                
                <form role="search" method="get" class="woocommerce-product-search" autocomplete="off" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                    <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
                    <input type="search" class="search-field" placeholder="חיפוש מוצרים..." value="<?php echo get_search_query(); ?>" name="s" required />
                    <button type="submit"></button>
                    <input type="hidden" name="post_type" value="product" />
                </form>
            </div>
        </nav>

        <nav class="mobile-navigation">
            <form role="search" method="get" class="woocommerce-product-search" autocomplete="off" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
                <input type="search" class="search-field" placeholder="חיפוש מוצרים..." value="<?php echo get_search_query(); ?>" name="s" required />
                <button type="submit"></button>
                <input type="hidden" name="post_type" value="product" />
            </form>
            <ul>
                <li>
                    <a href="<?=TL_SITE_URL;?>"> 
                        דף הבית
                    </a>
                </li>
                <?
                    if(!is_user_logged_in()) {
                ?>
                <li>
                    <a href="<?=esc_url(get_permalink( get_option('woocommerce_myaccount_page_id')));?>"> 
                        התחבר או הרשם
                    </a>
                </li>
                <?
                    }
                    else {
                        $endPoints = array(
                            "orders" => "הזמנות",
                            "downloads" => "הורדות",
                            "edit-address" => "כתובות",
                            "edit-account" => "פרטי חשבון",
                        );
                ?>
                <li class="dropdown">
                    <a href="javascript:;"> 
                        החשבון שלי
                    </a>
                    <ul>
                        <?
                            foreach($endPoints as $endPoint => $label) {
                        ?>
                        <a href="<?=esc_url(get_permalink( get_option('woocommerce_myaccount_page_id')));?>/<?=$endPoint;?>"> 
                            <?=$label;?>
                        </a>
                        <?
                            }
                        ?>
                        <a href="<?=esc_url(wc_logout_url());?>"> 
                            התנתק
                        </a>
                    </ul>
                </li>
                <?
                    }
                ?>
                <?
                    foreach($nav_items as $item) {

                        if($item->url == TL_SITE_URL || $item->url == TL_SITE_URL . "/")
                            continue;
                ?>
                <li>
                    <a href="<?=$item->url;?>"> 
                        <?=TL_Nav::get_title($item);?>
                    </a>
                </li>
                <?
                    }
                ?>
            </ul>
        </nav>
    </header>

    <div id="responsiveContainer">