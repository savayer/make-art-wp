<?php
    /**
     * Template Name: איך זה עובד?
     * TL Framework theme template
     * @author TL Interactive
     */

    the_post();

    get_header();
?>

<div id="page" class="single">

    <div class="container">
        
        <div class="row">
            <h1 class="section-title">
                <span><?=get_the_title();?></span>
            </h1>
        </div>

        <div class="page-content">
            <?
                the_content();
            ?>
        </div>

    </div>

    <?
        get_template_part("templates/index/section", "how-it-works");
    ?>

</div>

<?php

    get_footer();
?>