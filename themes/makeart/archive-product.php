<?php

/**
 * TL Framework theme template
 * @author TL Interactive
 */

global $term, $enqueueRelation;

$term = get_queried_object();

$display_type = get_woocommerce_term_meta($term->term_id, 'display_type', true) == "subcategories" ? "categories" : "products";

if($display_type == "products" && !isset($_GET['s']))
    $enqueueRelation = "archive";

$currentPage = (int)get_query_var('paged');
if($currentPage <= 0)
    $currentPage = 1;

if(isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] != 1)
    $currentPage = 1;

if($currentPage == 1) {

    get_header();
?>
<div id="page" class="archive single <?=$display_type;?>">

    <div class="container">
        
        <div class="row">
            <h1 class="section-title">
                <span><?
                if (is_search())
                    echo "תוצאות חיפוש עבור: <b>" . get_query_var("s") . "</b>";
                else if (is_shop())
                    echo "מוצרים";
                else
                    echo $term->name;
                ?></span>
            </h1>
        </div>
<?
}
else{
?>
<script type="text/javascript">
    MAX_PAGE_NUMBER = <?
        global $wp_query;
        echo (int)$wp_query->max_num_pages;
    ?>;
</script>
<?
}
    if($display_type == "products")
        get_template_part("templates/shop/archive", "products");
    else
        get_template_part("templates/shop/archive", "categories");

    if($currentPage == 1) {
?>
    </div>
    <div id="loading">
        <img src="<?=TL_CURRENT_THEME;?>/assets/img/logo.png" alt="" />
    </div>
</div>
<script type="text/javascript">
    var MAX_PAGE_NUMBER = <?
        global $wp_query;
        echo (int)$wp_query->max_num_pages;
    ?>;
    var CURRENT_PAGE = <?
        if(!isset($_GET['page']) || !is_numeric($_GET['page']) || $_GET['page'] < 1)
            echo 1;
        else
            echo (int)$_GET['page'];
    ?>;
    delete History;
</script>
<?php
    get_footer();
}