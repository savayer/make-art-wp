<?php

/**
 * TL Framework theme template
 * @author TL Interactive
 */

get_header();

get_template_part("templates/index/section", "categories");

get_template_part("templates/index/section", "how-it-works");

get_template_part("templates/index/section", "gallery");

get_template_part("templates/index/section", "videos");

get_template_part("templates/index/section", "customers");

get_template_part("templates/index/section", "contact");

get_footer();