<?php

class Product {

    public static $CROP_IMAGES = null;

    public static $product;

    public static $id;

    public static $image_price;

    public function __construct($id = null) {

        if($id == null)
            self::$id = get_the_ID();
        else
            self::$id = $id;

        self::$product = wc_get_product(self::$id);

        self::$CROP_IMAGES = json_decode(base64_decode(get_post_meta(self::$id, "crop-images", true)), true);

        foreach(self::$CROP_IMAGES as $key => $value)
            self::$CROP_IMAGES[$key]['url'] = str_replace("makeart.s125.upress.link", "make-art.co.il", $value['url']);

        self::$image_price = (int)get_post_meta(self::$id, "image-price", true);
        if(self::$image_price == -1)
            self::$image_price = (int)get_field("image-price", "option");
    }

    public static function calc_final_price() {
        
        $product_base_price = self::$product->get_price();
        $product_image_count = 0;

        foreach(self::$CROP_IMAGES as $image)
            if($image['has_image'])
                $product_image_count++;

        return ($product_base_price) + ($product_base_price / 100 * self::$image_price * $product_image_count);
    }
}