<?php

abstract class Cart {

    public static $allowedTypes = ["png", "jpg", "jpeg", "ico"];

    public static $maxSize = 15;

    public static function add_to_cart() {

        $_POST['p-id'] = (int)$_POST['p-id'];

        $product = new Product($_POST['p-id']);

        foreach(Product::$CROP_IMAGES as $index => $image) {
            Product::$CROP_IMAGES[$index]['text'] = implode(" | ", $_POST['text-' . $index]);
            Cart::upload_image($index);
            Cart::upload_preview_image($index);
        }

        WC()->cart->add_to_cart($_POST['p-id'], 1, null, array(
            "images" => Product::$CROP_IMAGES
        ), array(
            "images" => Product::$CROP_IMAGES
        ));

        wp_redirect(wc_get_cart_url());
    }

    public static function update_item_data($cart_item_data, $product_id) {
        $cart_item_data['final_price'] = Product::calc_final_price();
        return $cart_item_data;
    }

    public static function before_calculate_totals($cart_obj) {
        foreach($cart_obj->get_cart() as $key => $value)
            if(isset($value['final_price']))
                $value['data']->set_price($value['final_price']);
    }

    public static function upload_preview_image($index) {

        $upload_dir = wp_upload_dir();
        $basedir = $upload_dir['basedir'] . '/orders/preview/';

        $filename = "";

        do $filename = md5(microtime()) . ".png";
        while(file_exists($basedir . $filename));

        $fileStream = fopen($basedir . $filename, "w+");

        $streamData = base64_decode($_POST['preview-' . $index]);
        $streamData = explode(",", $streamData);
        $streamData = $streamData[1];

        fwrite($fileStream, base64_decode($streamData));

        fclose($fileStream);

        $attachmentID = wp_insert_attachment(array(
            'guid'           => $upload_dir['baseurl'] . '/orders/preview/' . $filename, 
            'post_mime_type' => "image/png",
            'post_title'     => preg_replace( '/\.[^.]+$/', '', $filename),
            'post_content'   => '',
            'post_status'    => 'inherit'
        ));

        update_post_meta($attachmentID, "front-upload", true);

        Product::$CROP_IMAGES[$index]['preview'] = $attachmentID;
    }

    public static function upload_image($index) {

        $file = $_FILES['file-' . $index];

        if($file['name'] == "")
            Product::$CROP_IMAGES[$index]['has_image'] = false;
        else {
            
            $fileType = explode(".", $file['name']);
            $fileType = $fileType[sizeof($fileType)-1];
            $fileSize = $file['size'] / 1024 / 1024;
            if(!in_array($fileType, self::$allowedTypes) || $fileSize > self::$maxSize) {
                wp_redirect(get_permalink($_POST['p-id']));
                die();
            }
            
            $upload_dir = wp_upload_dir();
            $basedir = $upload_dir['basedir'] . '/orders/images/';

            $filename = "";
            do $filename = md5(microtime()) . "." . $fileType;
            while(file_exists($basedir . $filename));

            move_uploaded_file($file['tmp_name'], $basedir . $filename);

            $attachmentID = wp_insert_attachment(array(
                'guid'           => $upload_dir['baseurl'] . '/orders/images/' . $filename, 
                'post_mime_type' => $file['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', $filename),
                'post_content'   => '',
                'post_status'    => 'inherit'
            ));

            update_post_meta($attachmentID, "front-upload", true);

            Product::$CROP_IMAGES[$index]['image'] = $attachmentID;
            Product::$CROP_IMAGES[$index]['has_image'] = true;
        }
    }
}

add_filter('woocommerce_add_cart_item_data', 'Cart::update_item_data', 10, 3);
add_action('woocommerce_before_calculate_totals', 'Cart::before_calculate_totals', 10, 1);