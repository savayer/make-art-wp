<?php

/**
 * Admin Classes
 */
require dirname(__FILE__) . "/admin/media.php";
require dirname(__FILE__) . "/admin/order.php";
require dirname(__FILE__) . "/admin/crop-settings.php";

add_action('admin_enqueue_scripts', function(){
    wp_enqueue_style('CUSTOM_ADMIN_CSS', TL_CURRENT_THEME . '/assets/css/admin.css', array());
});

/**
 * Front Classes
 */
require dirname(__FILE__) . "/product.php";
require dirname(__FILE__) . "/cart.php";