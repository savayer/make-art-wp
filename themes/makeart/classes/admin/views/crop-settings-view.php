<div id="crop_settings_tab_data" data-saved="<?=$cropImages;?>" class="panel woocommerce_options_panel hidden">
    <input type="hidden" name="crop-images" id="crop-images-input" value="" />
    <div class="options_group add-image-wrapper">
        <button type="button" class="add-image button-secondary button">
            הוסף תמונה
        </button>
    </div>
    <div class="image-list" data-template="PGEgaHJlZj0iamF2YXNjcmlwdDo7IiBjbGFzcz0iaW1hZ2UiIGRhdGEtaW5kZXg9Int7YXR0YWNobWVudC5pbmRleH19Ij4NCiAgICAgICAgICAgIDxpbWcgc3JjPSJ7e2F0dGFjaG1lbnQudXJsfX0iIC8+DQogICAgICAgIDwvYT4=">
        <?
            /*
            <a href="javascript:;" class="image" data-index="{{attachment.index}}">
                <img src="{{attachment.url}}" />
            </a>
         */
        ?>
    </div>
    <div class="image-price">
        <label for="image-price-input">
            מחיר יחסי עבור חריטת תמונה (באחוזים)
        </label>
        <input type="number" value="<?=$imagePrice;?>" name="image-price" id="image-price-input" min="0" max="100" placeholder="השאר ריק עבור ברירת המחדל" />
        <label for="image-color-input" style="margin-top: 15px;">
            צבע החריטה (HEX)
        </label>
        <input type="text" value="<?=$imageColor;?>" name="image-color" id="image-color-input" placeholder="השאר ריק עבור ברירת המחדל" />
        <label for="product-label" style="margin-top: 15px;">
            תווית מוצר:
        </label>
        <select name="product-label" id="product-label">
            <option value="" <? selected($productLabel == ""); ?>>ללא תווית</option>
            <option value="custom" <? selected($productLabel == "custom"); ?>>אפשרות בעיצוב אישי</option>
        </select>
        <label for="disable-image-upload" style="margin-top: 15px;">
            הוספת תמונה:
        </label>
        <select name="disable-image-upload" id="disable-image-upload">
            <option value="0" <? selected(!$disableImageUpload); ?>>פעיל</option>
            <option value="1" <? selected($disableImageUpload); ?>>כבוי</option>
        </select>
        <label for="disable-custom-text" style="margin-top: 15px;">
            הוספת טקסט:
        </label>
        <select name="disable-custom-text" id="disable-custom-text">
            <option value="0" <? selected(!$disableCustomText); ?>>פעיל</option>
            <option value="1" <? selected($disableCustomText); ?>>כבוי</option>
        </select>
        <label for="disable-sticker-picker" style="margin-top: 15px;">
            הוספת אייקון:
        </label>
        <select name="disable-sticker-picker" id="disable-sticker-picker">
            <option value="0" <? selected(!$disableStickerPicker); ?>>פעיל</option>
            <option value="1" <? selected($disableStickerPicker); ?>>כבוי</option>
        </select>
    </div>
    <div class="image-settings">
        <div class="image-settings-inner">
            <div class="image-cropper">
                <h3 class="title">תחם את איזור ההדפסה על התמונה:</h3>
                <div class="image-cropper-inner">
                    <img src="" alt="" />
                </div>
            </div>
            <div class="extra-settings">
                <?
                    woocommerce_wp_text_input( 
                        array( 
                            'id'            => 'image-label', 
                            'placeholder'   => '',
                            'label'         => 'תווית התמונה',
                            'description'   => 'על מנת שתדע שמדובר בתמונה זו'
                        )
                    );
                    woocommerce_wp_select( 
                        array( 
                            'id'            => 'image-order',
                            'placeholder'   => '',
                            'label'         => 'מיקום התמונה',
                            'description'   => 'בחר את מיקום התמונה ביחס לתמונות האחרות',
                            'options'       => array()
                        )
                    );
                ?>
            </div>
            <div class="actions">
                <button type="button" class="remove button-secondary button">
                    מחק תמונה
                </button>
                <button type="button" class="save button-primary button">
                    שמור תמונה
                </button>
                <button type="button" class="cancel button-secondary button">
                    בטל שינויים
                </button>
            </div>
        </div>
    </div>
</div>