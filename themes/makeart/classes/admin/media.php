<?php

abstract class MediaAdjusments {

    public static function hide_media_on_modal($args) {

        if (!is_admin()) {
            return;
        }

        $args['meta_query'] = [
            [
                'key' => 'front-upload',
                'compare' => 'NOT EXISTS',
            ]
        ];

        return $args;
    }

    public static function hide_media_on_list($query) {

        if (!is_admin()) {
            return;
        }

        if (!$query->is_main_query()) {
            return;
        }

        $screen = get_current_screen();
        if (!$screen || 'upload' !== $screen->id || 'attachment' !== $screen->post_type) {
            return;
        }

        $query->set('meta_query', [
            [
                'key' => 'front-upload',
                'compare' => 'NOT EXISTS',
            ]
        ]);

        return;
    }

}

add_filter('ajax_query_attachments_args', 'MediaAdjusments::hide_media_on_modal');
add_action('pre_get_posts', 'MediaAdjusments::hide_media_on_list' );