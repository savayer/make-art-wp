<?php

abstract class Order {

    public static function item_meta_data($item_id, $item, $_product) {
        if($_product != null) {
            foreach($item['images'] as $image) {
                echo "<br />";
                echo $image['label'] . ": ";
                echo '<a href="' . esc_url(wp_get_attachment_url($image['preview'])) . '" target="_blank">תצוגה מקדימה</a>';
                if($image['has_image'])
                    echo ' | <a href="' . esc_url(wp_get_attachment_url($image['image'])) . '" target="_blank">תמונה מקורית</a>';
                if($image['text'] != "")
                    echo ' | טקסט: ' . $image['text'];
            }
        }
    }
}

add_action( 'woocommerce_before_order_itemmeta', 'Order::item_meta_data', 10, 3 );