<?php

abstract class CropSettings {

    public static function save($post_id) {
        update_post_meta($post_id, "crop-images", $_POST['crop-images']);

        if($_POST['image-price'] == "")
            $_POST['image-price'] = -1;

        update_post_meta($post_id, "image-price", (int)$_POST['image-price']);
        update_post_meta($post_id, "image-color", $_POST['image-color']);
        update_post_meta($post_id, "product-label", $_POST['product-label']);
        update_post_meta($post_id, "disable-image-upload", $_POST['disable-image-upload']);
        update_post_meta($post_id, "disable-custom-text", $_POST['disable-custom-text']);
        update_post_meta($post_id, "disable-sticker-picker", $_POST['disable-sticker-picker']);
    }

    public static function tab() {
        ?>
        <li class="crop_settings_tab"><a href="#crop_settings_tab_data">הגדרות תמונות</a></li>
        <?
    }

    public static function view() {

        $cropImages = "";
        $imagePrice = "";
        $imageColor = "";

        if(isset($_GET['post'])) {
            $cropImages = get_post_meta($_GET['post'], "crop-images", true);
            $imagePrice = (int)get_post_meta($_GET['post'], "image-price", true);
            $imageColor = get_post_meta($_GET['post'], "image-color", true);
            $productLabel = get_post_meta($_GET['post'], "product-label", true);
            $disableImageUpload = (bool)get_post_meta($_GET['post'], "disable-image-upload", true);
            $disableCustomText = (bool)get_post_meta($_GET['post'], "disable-custom-text", true);
            $disableStickerPicker = (bool)get_post_meta($_GET['post'], "disable-sticker-picker", true);

            if($imagePrice == -1)
                $imagePrice = "";
        }

        require dirname(__FILE__) . "/views/crop-settings-view.php";
    }
}

add_filter('woocommerce_product_write_panel_tabs', "CropSettings::tab");
add_filter('woocommerce_product_data_panels', "CropSettings::view");
add_action("woocommerce_process_product_meta", "CropSettings::save", 10, 3);
add_action('admin_enqueue_scripts', function(){
    wp_enqueue_style('CROPPER_JS', TL_CURRENT_THEME . '/assets/css/cropper.css');
    wp_enqueue_script('CROPPER_JS', TL_CURRENT_THEME . '/assets/js/cropper.js', array("jquery"));
    wp_enqueue_script('ADMIN_CROPPER_JS', TL_CURRENT_THEME . '/assets/js/admin-cropper.js', array("jquery", "CROPPER_JS"));
});