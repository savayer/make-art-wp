<?php
    /**
     * TL Framework theme template
     * @author TL Interactive
     */

    the_post();

    get_header();
?>

    <div id="page" class="single">

        <div class="container">

            <div class="row">
                <h1 class="section-title">
                    <span><?=get_the_title();?></span>
                </h1>
            </div>

            <div class="page-content">
                <?
                the_content();
                ?>
            </div>

        </div>

    </div>

<?php
    get_footer();
?>