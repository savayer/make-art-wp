<div class="row categories-row">
<?php

    global $term;

    $categories = get_categories(apply_filters('woocommerce_product_subcategories_args', array(
        'parent' => $term->term_id,
        'menu_order' => 'ASC',
        'hide_empty' => 0,
        'hierarchical' => 1,
        'taxonomy' => 'product_cat',
        'pad_counts' => 1
    )));

    foreach ($categories as $category) {
        ?>
        <div class="col-md-3 category-wrapper">
            <?
                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );
            ?>
            <a href="<?=esc_url(get_term_link($category));?>" class="category">
                <img class="category-thumbnail" src="<?=$image;?>" alt="" />
                <h5 class="category-title"><?=$category->name;?></h5>
            </a>
        </div>
        <?
    }
?>
</div>