<section class="categories">

    <div class="container">

        <div class="row">
            <h3 class="section-title">
                <span>הקטגוריות החמות שלנו</span>
            </h3>
        </div>

        <?
            $args = array(
                'taxonomy'      => "product_cat",
                'number'        => null,
                'hide_empty'    => false,
                'parent'        => 0
            );

            $categories = get_terms($args);
            $catPages = ceil((sizeof($categories) - 1) / 9);
        ?>

        <div id="categories-carousel" class="carousel slide" data-ride="carousel">
            <?
                if($catPages > 1) {
            ?>
            <ol class="carousel-indicators">
                <?
                    for($i = 0; $i < $catPages; $i++) {
                ?>
                <li data-target="#categories-carousel" data-slide-to="<?=$i;?>" class="<?=$i == 0?("active"):("");?>"></li>
                <?
                    }
                ?>
            </ol>
            <?
                }
            ?>
            <div class="carousel-inner">

                <?

                    $categoriesCount = 0;
                    $carouselItems = 0;
                    $rowBreaks = array(0, 3, 5, 9);
                    $catClasses = array(
                        "col-lg-3 col-sm-6",
                        "col-lg-6 col-sm-6",
                        "col-lg-3 col-sm-12",
                        "col-lg-6 col-sm-6",
                        "col-lg-6 col-sm-6",
                        "col-lg-3 col-sm-6",
                        "col-lg-3 col-sm-6",
                        "col-lg-3 col-sm-6",
                        "col-lg-3 col-sm-6"
                    );

                    foreach($categories as $category) {

                        if($category->name == "Uncategorized")
                            continue;

                        $categoriesCount++;

                        if($categoriesCount == 1) {
                            $carouselItems++;
                            ?>
                        <div class="carousel-item <?=$carouselItems == 1 ? ("active"):("");?>">
                            <div class="categories-carousel-block">
                            <?
                        }

                        if(array_search($categoriesCount - 1, $rowBreaks) !== FALSE)
                            echo '<div class="row categories-row">';

                        ?>
                        <div class="<?=$catClasses[$categoriesCount-1];?> category-wrapper">
                            <?
                                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                                $image = wp_get_attachment_url( $thumbnail_id );
                            ?>
                            <a href="<?=esc_url(get_term_link($category));?>" class="category">
                                <img class="category-thumbnail" src="<?=$image;?>" alt="" />
                                <h5 class="category-title"><?=$category->name;?></h5>
                            </a>
                        </div>
                        <?

                        if(array_search($categoriesCount, $rowBreaks) !== FALSE)
                            echo '</div>';

                        if($categoriesCount == 9) {
                            $categoriesCount = 0;
                            ?>
                            </div>
                        </div>
                            <?
                        }
                    }

                    if(array_search($categoriesCount, $rowBreaks) === FALSE)
                        echo "</div>";

                    if($categoriesCount > 0)
                        echo "</div></div>";
                ?>
            </div>
            <?
                if($catPages > 1) {
            ?>
            <a class="carousel-control-prev" href="#categories-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#categories-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <?
                }
            ?>
        </div>

    </div>

</section>