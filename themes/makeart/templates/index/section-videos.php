<?
    $query = new WP_Query(array(
        "post_type" => "video",
        "posts_per_page" => 3 
    ));

    global $videoCount;

    $videoCount = 0;

    if($query->have_posts()) {
?>
<section id="videos">

    <div class="container">

        <div class="row">
            <h3 class="section-title">
                <span>סרטונים</span>
            </h3>
        </div>

        <div class="row videos-row">

            <div class="col-md-6">
                <?
                    TL_Loop::start_loop(function(){

                        global $videoCount, $videoSize;
                        
                        $videoSize = "";
                        $videoCount++;

                        if($videoCount == 3) {
                            $videoSize = "large";
                            echo '</div><div class="col-md-6">';
                        }

                        get_template_part("templates/posts/video", "single");

                    }, $query);
                ?>
            </div>
        </div>

        <div class="show-more">
            <a href="<?=esc_url(get_field("page-videos", "option"));?>" class="button border-button blue">לכל הסרטונים</a>
        </div>

    </div>

</section>
<?
    }