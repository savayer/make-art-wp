<section id="gallery">

    <div class="container">
        <div class="row">
            <h3 class="section-title">
                <span>גלריית תמונות ועיצובים</span>
            </h3>
        </div>
    </div>

    <?
        get_template_part("templates/posts/gallery", "loop");
    ?>

    <div class="show-more">
        <a href="<?=esc_url(get_field("page-gallery", "option"));?>" class="button border-button blue">לכל העבודות</a>
    </div>

</section>