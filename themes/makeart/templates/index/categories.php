<section class="categories">

    <div class="container">

        <div class="row">
            <h3 class="section-title">
                <span>הקטגוריות החמות שלנו</span>
            </h3>
        </div>

        <div class="categories-carousel">
            <div class="arrows">
                <a href="#" class="right"></a>
                <a href="#" class="left"></a>
            </div>

            <div class="categories-carousel-block">
                <div class="row categories-row">
                    <div class="col-md-3 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category1.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                    <div class="col-md-6 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category2.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                    <div class="col-md-3 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category1.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                </div>

                <div class="row categories-row">
                    <div class="col-md-6 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category1.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                    <div class="col-md-6 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category2.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                </div>

                <div class="row categories-row">
                    <div class="col-md-3 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category1.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                    <div class="col-md-3 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category2.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                    <div class="col-md-3 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category1.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                    <div class="col-md-3 category-wrapper">
                        <a href="#" class="category">
                            <img class="category-thumbnail" src="<?= TL_CURRENT_THEME; ?>/assets/img/demos/category2.png" alt="" />
                            <h5 class="category-title">צריבה על עץ</h5>
                        </a>
                    </div>
                </div>
            </div>

            <div class="carousel-pagination">
                <a href="#" class="active"></a>
                <a href="#"></a>
                <a href="#"></a>
            </div>
        </div>
    </div>

</section>