<?
    $customers = array();

    while(have_rows("customers")) {
        the_row();
        $customers[] = get_sub_field("customer-logo");
    }
    
    if(!empty($customers)) {
        $customersPages = ceil((sizeof($customers) - 1) / 18);
?>
<section id="customers">

    <div class="container">

        <div class="row">
            <h3 class="section-title">
                <span>מבין לקוחותינו</span>
            </h3>
        </div>

        <div id="customers-carousel" class="carousel slide" data-ride="carousel">
            <?
                if($customersPages > 1) {
            ?>
            <ol class="carousel-indicators">
                <?
                    for($i = 0; $i < $customersPages; $i++) {
                ?>
                <li data-target="#customers-carousel" data-slide-to="<?=$i;?>" class="<?=$i == 0?("active"):("");?>"></li>
                <?
                    }
                ?>
            </ol>
            <?
                }
            ?>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?
                        $customersCount = 0;
                        foreach($customers as $key => $customer) {

                            if($customersCount == 19)
                                $customersCount = 1;

                            $customersCount++;

                            if($customersCount == 1)
                                echo '<div class="customers-row row d-flex justify-content-between">';

                            if($customersCount == 10)
                                echo '</div><div class="customers-row row d-flex justify-content-between">';

                            if($customersCount == 19)
                                echo '</div></div><div class="carousel-item"><div class="customers-row row d-flex justify-content-between">';
                    ?>
                    <img src="<?=esc_url($customer['url']);?>" alt="" />
                    <?
                        }

                        if($customersCount != 19)
                            echo "</div>";
                    ?>
                </div>
            </div>
            <?
                if($customersPages > 1) {
            ?>
            <a class="carousel-control-prev" href="#customers-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#customers-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <?
                }
            ?>
        </div>
    </div>
</section>
<?
    }