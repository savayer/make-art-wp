<section class="how-it-works">

    <div class="container">

        <div class="row">
            <h3 class="section-title">
                <span>איך זה עובד?</span>
            </h3>
        </div>

        <div class="steps row">

            <?
                $count = 0;

                while(have_rows("how-it-works", "option")) {

                    the_row();

                    $count++;
            ?>
            <div class="col-md-4 col-sm-6 <?

                if($count == 1)
                    echo "text-right";
                else if($count == 2)
                    echo "text-center";
                else
                    echo "text-left";

                $image = get_sub_field("step-image");

            ?>">
                <div class="step">
                    <img src="<?=$image['url'];?>" alt="<?=esc_attr(get_sub_field("step-title"));?>" />
                    <h5 class="step-title"><?=get_sub_field("step-title");?></h5>
                    <p class="step-desc"><?=get_sub_field("step-content");?></p>
                </div>
            </div>
            <?
                }
            ?>

        </div>

        <div class="materials">
            <ul class="row">
                <?
                    while(have_rows("materials", "option")) {
                        the_row();

                        $image = get_sub_field("image");
                ?>
                <li class="col-lg-2 col-md-4 col-sm-6">
                    <div class="material">
                        <img src="<?=$image['url'];?>" alt="<?=esc_attr(get_sub_field("name"));?>" />
                        <h5 class="title"><?=get_sub_field("name");?></h5>
                    </div>
                </li>
                <?
                    }
                ?>
            </ul>
        </div>
    </div>
</section>