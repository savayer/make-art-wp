<?
    global $videoSize;

    $video = get_field("video-url");
    $videoParts = explode("?", $video);
    parse_str($videoParts[1], $videoParams);
?>
<a href="javascript:;" data-video-id="<?=$videoParams['v'];?>" class="video <?=$videoSize;?>">
    <img src="<?=TL_General::youtube_get_thumbnail($video);?>" alt="" />
    <p class="video-info">
        <span class="title"><?=get_the_title();?></span>
        <?
            $customer = get_field("customer-name");
            if($customer != "") {
        ?>
        <span class="customer">| שם הלקוח: <?=$customer;?></span>
        <?
            }
        ?>
    </p>
</a>