<?
    $product = wc_get_product($post->ID);
    $categories = get_the_terms($post->ID, 'product_cat');
    $image = wp_get_attachment_image(get_post_meta($post->ID, "_thumbnail_id", true), 'full');
    $label = get_post_meta($post->ID, "product-label", true);
?>
<a href="<?=esc_url(get_the_permalink());?>" class="labeled-<?=$label;?> product col-xl-3 col-lg-4 col-md-6 col-xs-12">
    <div class="img"><?= $image; ?></div>
    <div class="inner">
        <span class="title"><?= $post->post_title; ?></span>
        <span class="cat"><?= $categories[sizeof($categories) - 1]->name; ?></span>
        <span class="price">מחיר
            <?php if ($product->get_regular_price() != $product->get_price()):
            ?>
                <span style="text-decoration:line-through;">₪<?=$product->get_regular_price();?></span>
            <?php endif;?>
            <b>₪<?= $product->get_price(); ?></b></span>
    </div>
</a>