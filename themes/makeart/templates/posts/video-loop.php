<div class="row videos-row">
    <?
    global $videoSize;
    $videoSize = "medium";

    $currentPage = isset($_GET['paginiation']) ? (int)$_GET['paginiation'] : 1;

    $query = new WP_Query(array(
        "post_type" => "video",
        "posts_per_page" => 12,
        "paged" => $currentPage
    ));

    TL_Loop::start_loop(function () {
        ?>
        <div class="col-md-4">
            <?
            get_template_part("templates/posts/video", "single");
            ?>
        </div>
        <?
    }, $query);
    ?>
</div>
<?
    if($query->max_num_pages > 1) {
?>
<div id="paginiation">
    <?
        echo paginate_links(array(
            'format' => '?paginiation=%#%',
            'total' => $query->max_num_pages,
            'current' => $currentPage
        ));
    ?>
</div>
<?
    }
?>