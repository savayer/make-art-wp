<?

    if(is_front_page())
        $limit = -1;
    else
        $limit = 12;

    $currentPage = isset($_GET['paginiation']) ? (int)$_GET['paginiation'] : 1;

    $query = new WP_Query(array(
        "post_type" => "gallery",
        "posts_per_page" => $limit,
        "paged" => $currentPage
    ));

    global $galleryCount;

    $galleryCount = 0;
    
    echo '<div class="row gallery-row">';

    TL_Loop::start_loop(function() {
        
        global $galleryCount;

        $galleryCount++;


        get_template_part("templates/posts/gallery", "single");

    }, $query);

    echo '</div>';

    if(!is_front_page() && $query->max_num_pages > 1) {
?>
<div class="container">
    <div id="paginiation">
        <?
            echo paginate_links(array(
                'format' => '?paginiation=%#%',
                'total' => $query->max_num_pages,
                'current' => $currentPage
            ));
        ?>
    </div>
</div>
<?
    }