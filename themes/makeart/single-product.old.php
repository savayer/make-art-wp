<?php

/**
 * TL Framework theme template
 * @author TL Interactive
 */

the_post();

global $enqueueRelation;

$enqueueRelation = "product";

new Product();

if(isset($_POST['p-id'])) {
    Cart::add_to_cart();
    die();
}

get_header();
?>
<section id="product" class="layer-0" data-color="<?=get_post_meta(get_the_ID(), "image-color", true);?>">

    <div class="container">

        <div class="row">
            <h1 class="section-title">
                <span><?= get_the_title(); ?></span>
            </h1>

            <div class="row product-row">

                <div class="col-md-5">

                    <div class="gallery">

                        <div class="block-title">
                            <h3 class="product-name"><?= get_the_title(); ?></h3>
                            <span class="manufactor">יצרן: LaerArt</span>
                            <span>מקט: <?=Product::$product->get_sku();?></span>
                        </div>

                        <div class="large-image">

                            <img src="<?= Product::$CROP_IMAGES[0]['url']; ?>" alt="" />

                            <div class="canvas-wrapper">
                                <?
                                foreach (Product::$CROP_IMAGES as $index => $image) {

                                    $originalWidth = $image['crop']['width'];
                                    $originalHeight = $image['crop']['height'];

                                    $croppedX = (100 / $originalWidth) * $image['crop']['data']['x'];
                                    $croppedY = (100 / $originalHeight) * $image['crop']['data']['y'];
                                    $croppedWidth = (100 / $originalWidth) * $image['crop']['data']['width'];
                                    $croppedHeight = (100 / $originalHeight) * $image['crop']['data']['height'];
                                    ?>
                                <div class="canvas-layer layer-<?=$index;?>-related layer-related" data-image="<?=base64_encode($image['url']);?>" data-index="<?=$index;?>" data-width="<?=$croppedWidth;?>" data-height="<?=$croppedHeight;?>" data-x="<?=$croppedX;?>" data-y="<?=$croppedY;?>">
                                    <canvas id="canvas-layer-<?=$index;?>"></canvas>
                                </div>
                                <?
                            }
                            ?>
                            </div>
                        </div>

                        <?
                        if (sizeof(Product::$CROP_IMAGES) > 1) {
                            ?>
                        <div class="row gallery-images">
                        <?
                        foreach (Product::$CROP_IMAGES as $index => $image) {
                            ?>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="image" data-index="<?=$index;?>">
                                    <img src="<?= $image['url']; ?>" alt="" />
                                </div>
                            </div>
                            <?
                        }
                        ?>
                        </div>
                        <?
                    }
                    ?>

                    </div>

                    <?
                        if(get_the_content() != "") {
                    ?>
                    <div class="description desktop-description">

                        <h6 class="block-title">תיאור המוצר</h6>

                        <div class="content">
                            <?
                            the_content();
                            ?>
                        </div>

                    </div>
                    <?
                        }
                    ?>

                </div>

                <div class="col-md-7">

                    <form id="product-form" action="" method="POST" enctype="multipart/form-data">

                        <input type="hidden" name="p-id" value="<?=Product::$id;?>" />

                        <?
                        foreach (Product::$CROP_IMAGES as $index => $image) {
                            ?>

                        <input type="hidden" id="preview-<?=$index;?>" name="preview-<?=$index;?>" value="" />

                        <div class="layer-<?= $index; ?>-related layer-related">
                                
                            <div class="image-upload empty order-step" data-index="<?=$index;?>">

                                <h5 class="block-title step" data-step="1">
                                    העלה תמונה
                                </h5>

                                <div class="step-box-content">

                                    <div class="settings">

                                        <p class="info">
                                            * עם העלאת התמונה אני מאשר שזכויות התמונה שייכות לי
                                        </p>

                                        <div class="inputs">

                                            <div class="stauration-container">
                                                <button type="button" class="control minus">-</button>
                                                <input type="range" min="-0.4" step="0.025" max="0.65" value="0" class="gamma-input slider" />
                                                <button type="button" class="control plus">+</button>
                                            </div>

                                            <button type="button" class="invert-button button simple blue">היפוך סיטורציה</button>
                                            
                                            <div class="file">
                                                <input type="file" name="file-<?=$index;?>" accept=".jpeg,.jpg,.png,.ico" />
                                                <span class="button simple blue">צרף קובץ</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="preview">
                                        <img src="" alt="" />
                                    </div>
                                </div>

                            </div>

                            <div class="custom-text order-step" data-index="<?=$index;?>">

                                <h5 class="block-title step" data-step="2">
                                    הכנס טקסט
                                </h5>

                                <div class="step-box-content">
                                    <label for="your-text-<?=$index;?>">
                                        הטקסט שלך כאן:
                                    </label>
                                    <textarea id="your-text-<?=$index;?>" name="text-<?=$index;?>"></textarea>
                                </div>
                            </div>
                        </div>
                        <?
                        }
                        ?>
                        <?
                            if(get_the_content() != "") {
                        ?>
                        <div class="description mobile-description">

                            <h6 class="block-title">תיאור המוצר</h6>

                            <div class="content">
                                <?
                                the_content();
                                ?>
                            </div>

                        </div>
                        <?
                            }
                        ?>
                        <div class="summary">

                            <div class="price" id="price" data-base="<?=Product::$product->get_price();?>" data-image="<?=Product::$image_price;?>">
                                סה"כ <span>₪<?=number_format(Product::$product->get_price(), 2, ".", "");?></span>
                                <?php if (Product::$product->get_regular_price() > 0):
                                ?>
                                <span class="regular-price">₪<?=number_format(Product::$product->get_regular_price(), 2, ".", "");?></span>
                                <?php endif;?>
                            </div>

                            <button type="submit" class="button simple blue bold">הוסף לעגלת הקניות</button>
                            
                        </div>

                    </form>

                </div>

            </div>
        </div>

    </div>

</section>
<?php
get_footer();
?>