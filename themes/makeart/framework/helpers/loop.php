<?php



abstract class TL_Loop {
    
    /**
     * Generates a date string of the post creation date
     * @return string Date string
     */
    public static function date_string() {
        
        return TL_Date::date_string(get_the_time("U"));
    }
    
    /**
     * Returns short version of the post description
     * @param string $length Final length of the description
     * @return string Short description
     */
    public static function excerpt($length = 100) {
        
        return TL_String::short(get_the_content(), $length);
    }
    
    /**
     * Performing the WordPress posts loop in much easier way
     * @param function $callback Function callback to handle each iteration
     * @param WP_Query $query Query handler, if null (default) the handler will be the main handler
     * @return void
     */
    public static function start_loop($callback, &$query = null) {
        
        if($query != null)
            while($query->have_posts()) {
                $query->the_post();
                $callback();
            }
        else
            while(have_posts()) {
                the_post();
                $callback();
            }
            
        wp_reset_postdata();
    }
}