<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class TL_Social {
    
    public static $url;
    
    public static $title;
    
    public static $description;
    
    public static $image;
    
    public static function init_social_settings() {
        
        add_action("wp", function(){

            the_post();

            self::$url            = esc_url(get_the_permalink());
            self::$title          = esc_attr(get_the_title());
            self::$description    = esc_attr(str_replace("\n", "", trim(strip_tags(get_the_content()))));
            self::$image          = "http://p17767-125-2949.s125.upress.link/wp-content/themes/tl/assets/img/logo.png";
            
            if(is_single() && get_post_type() == "post") {
                
                $cover = get_field("cover");
                if($cover != null)
                    self::$image = $cover['url'];
                else
                    self::$image = TL_Post::get_thumbnail_url();
            }
            
            self::$image = esc_url(self::$image);
        });
        
        add_action("wp_head", function() {
            
            echo '
<meta property="og:url"           content="' . self::$url . '" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="' . self::$title . '" />
<meta property="og:description"   content="' . self::$description . '" />
<meta property="og:image"         content="' . self::$image . '" />';
        });
    }
    
    public static function get_share_link($network) {
        
        switch($network) {
            case "facebook":
                return esc_url("https://www.facebook.com/dialog/share?" . http_build_query(array(
                    "app_id"        => TL_Framework::$config['SOCIAL']['FACEBOOK_APP_ID'],
                    "display"       => "popup",
                    "href"          => self::$url,
                    "redirect_uri"  => self::$url
                )));
            case "twitter":
                return esc_url("https://twitter.com/share?" . http_build_query(array(
                    "url"   => self::$url,
                    "text"  => self::$title
                )));
            case "google-plus":
                return esc_url("https://plus.google.com/share?" . http_build_query(array(
                    "url"   => self::$url
                )));
            case "whatsapp":
                
                $args = http_build_query(array(
                    "text"   => self::$title . ": " . self::$url
                ));
                
                if(wp_is_mobile())
                    return "whatsapp://send?" . $args;
                
                return "https://web.whatsapp.com/send?" . $args; 
        }
    }
}