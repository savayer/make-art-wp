<?php


abstract class TL_String {
    
    /**
     * Takes a string and return short version of it
     * @param type $str String to shorten up
     * @param type $length Length of the final string
     * @return string Shorten string
     */
    public static function short($str, $length, $strip_tags = true) {
        
        if($strip_tags)
            $str = strip_tags($str);
        
        return mb_substr($str, 0, $length, 'UTF-8');
    }
    
    /**
     * Return the string with codes for quotation marks
     * @param string $str Original string
     * @return string Escaped string
     */
    public static function esc_attr($str) {
        
        $str = str_replace('"', '&quot;', $str);
        $str = str_replace("'", '&#39;', $str);
        
        return $str;
    }
}
