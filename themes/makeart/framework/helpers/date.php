<?php


abstract class TL_Date {
    
    /**
     * Array of Hebrew months name as a base for translation
     * @var string[]
     */
    public static $months = array(
        "ינואר",
        "פברואר",
        "מרץ",
        "אפריל",
        "מאי",
        "יוני",
        "יולי",
        "אוגוסט",
        "ספטמבר",
        "אוקטובר",
        "נובמבר",
        "דצמבר"
    );
    
    /**
     * Generates a date string by the the timestamp
     * @param int $timestamp Unix timestamp to generate a string from
     * @return string Date string
     */
    public static function date_string($timestamp) {
        
        $date = sprintf(
            "%d %s%s, %d",
            date("d", $timestamp),
            __("ב", TL_LANGUAGE_SLUG . "_date"),
            __(self::$months[(int)date("m", $timestamp) - 1], TL_LANGUAGE_SLUG . "_date"),
            date("Y", $timestamp)
        );
        
        return $date;
    }
}