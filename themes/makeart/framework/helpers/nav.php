<?php



abstract class TL_Nav {
    
    /**
     * Returns the ID of a menu based on the location
     * @param type $location Location of the navigation bar
     * @return int ID of the menu
     */
    public static function get_menu($location) {
        
        $locations = get_nav_menu_locations();
        
        $menu = get_term($locations[$location], 'nav_menu');

        return $menu->term_id;
    }
    
    /**
     * Return menu item title
     * @param WP_Post $item Menu item
     * @return string Menu item title
     */
    public static function get_title(&$item) {
        
        if($item->post_title == "")
            return get_the_title((int)$item->object_id);
        
        return $item->post_title;
    }
    
    /**
     * Return the class name or TRUE if the menu item is the current page
     * @global WP_Post $post
     * @param WP_Post $item Menu item to check if it's the current page
     * @param string $classname Class name to return if it's the current page
     * @return boolean|string
     */
    public static function current_page(&$item, $classname = "") {
        
        global $post;
        
        $post_id = $post->ID;
        
        if(is_search() || is_archive() || (is_single() && get_post_type() == "post") || is_home())
            $post_id = (int)get_option("page_for_posts");
        
        if($item->object_id == $post_id) {
            
            if($classname == "")
                return true;
            
            return $classname;
        }
        
        if($classname == "")
            return false;
        
        return "";
    }
    
}