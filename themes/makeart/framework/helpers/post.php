<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class TL_Post {
    
    /**
     * Retrieve a list of related posts by tags and categories
     * @param int $limit Related posts limit
     * @param int $post_id Post id if needed
     * @return WP_Post[]
     */
    public static function related_posts($limit = 4, $post_id = null) {
        
        if($post_id == null)
            $post_id = get_the_ID();
        
        $tags = get_the_tags($post_id);
        $cats = get_the_category($post_id);
        $args = array(
            "posts_per_page"    => $limit,
            "post__not_in"      => array($post_id),
            "orderby"           => "rand",
            "post_status"       => "publish",
            "tax_query"         => array(
                "relation" => "OR"
            )
        );
        
        if(is_array($tags))
            foreach($tags as $tag)
                $args['tax_query'][] = array(
                    "taxonomy"  => "post_tag",
                    "field"     => "term_id",
                    "terms"     => $tag->term_id
                );
        
        if(is_array($cats))
            foreach($cats as $cat)
                $args['tax_query'][] = array(
                    "taxonomy"  => "category",
                    "field"     => "term_id",
                    "terms"     => $cat->term_id
                );
        
        return new WP_Query($args);
    }
    
    /**
     * Returns thumbnail URL if defined, otherwise returns default thumbnail.
     * @param type $post_id Post id if needed
     * @return string
     */
    public static function get_thumbnail_url($post_id = null) {
        
        $url = get_the_post_thumbnail_url($post_id);
        
        if($url == "" && isset(TL_Framework::$config['DEFAULT_THUMBNAIL']))
            return TL_Framework::$config['DEFAULT_THUMBNAIL'];
        
        return $url;
    }
    
    /**
     * Adds a view to the post view count
     * @param type $post_id Post id if needed
     * @return void
     */
    public static function add_view($post_id = null) {
        
        if($post_id == null)
            $post_id = get_the_ID();
        
        if(!isset($_SESSION['post-views-' . $post_id])) {
            
            $_SESSION['post-views-' . $post_id] = true;
            
            $post_views = (int)get_post_meta($post_id, "views", true);
            $post_views++;
            
            update_post_meta($post_id, "views", $post_views);
        }
    }
    
    /**
     * Retrieves the post view count
     * @param type $post_id Post id if needed
     * @return int Post view count
     */
    public static function get_views($post_id = null) {
        
        if($post_id == null)
            $post_id = get_the_ID();
        
        return (int)get_post_meta($post_id, "views", true);
    }
}