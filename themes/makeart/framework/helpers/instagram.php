<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class TL_Instagram {
    
    const API_ENDPOINT = "https://api.instagram.com";
    
    public static function auth(callable $callback) {
        
        if(isset($_GET['code']) && $_GET['code'] != "") {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, self::API_ENDPOINT . "/oauth/access_token");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                "client_id"     => TL_Framework::$config['INSTAGRAM']['client_id'],
                "client_secret" => TL_Framework::$config['INSTAGRAM']['client_secret'],
                "redirect_uri"  => TL_Framework::$config['INSTAGRAM']['redirect_url'],
                "grant_type"    => "authorization_code",
                "code"          => $_GET['code']
            )));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            
            curl_close($ch);
            
            $data = json_decode($result, true);
            
            $callback($data['access_token'], $data['user']);
        }
    }
    
    public static function get_auth_link() {
        
        return self::API_ENDPOINT . "/oauth/authorize/?" . http_build_query(array(
            "client_id"     => TL_Framework::$config['INSTAGRAM']['client_id'],
            "redirect_uri"  => TL_Framework::$config['INSTAGRAM']['redirect_url'],
            "scope"         => "public_content",
            "response_type" => "code"
        ));
    }
    
    public static function get_recent_media($limit = 9, $access_token = null) {
        
        if($access_token == null)
            $access_token = TL_Framework::$config['INSTAGRAM']['client_main_access'];
        
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, self::API_ENDPOINT . "/v1/users/self/media/recent?" . http_build_query(array(
            "access_token"  => $access_token,
            "count"         => $limit
        )));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($ch);
        
        curl_close($ch);
        
        $data = json_decode($result, true);
        
        return $data['data'];
    } 
    
}