<?php

abstract class TL_General {

    public static function youtube_get_thumbnail($url) {

        $videoParts = explode("?", $url);
        parse_str($videoParts[1], $videoParams);
        
        return "https://img.youtube.com/vi/" . $videoParams['v'] . "/maxresdefault.jpg";
    }
    
    public static function breadcrumbs() {

        // Get the query & post information
        global $post, $wp_query;
        
        $links = array();

        // Do not display on the homepage
        if ( !is_front_page() ) {

            // Home page
            $links[] = array(
                "is_home"   => true,
                "url"       => get_home_url(),
                "label"     => ""
            );
            
            if(is_home())
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => get_the_title(get_option('page_for_posts'))
                );

            if ( is_archive() && !is_tax() && !is_category() && !is_tag()) {
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => post_type_archive_title($prefix, false)
                );
            } 
            else if(is_archive() && is_tax() && !is_category() && !is_tag()) {

                // If post is a custom post type
                $post_type = get_post_type();

                // If it is a custom post type display name and link
                if($post_type != 'post') {

                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);

                    $links[] = array(
                        "is_home"   => false,
                        "url"       => $post_type_archive,
                        "label"     => $post_type_object->labels->name
                    );
                }

                $custom_tax_name = get_queried_object()->name;

                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => $custom_tax_name
                );
                
            } 
            else if(is_single()) {
                

                // If post is a custom post type
                $post_type = get_post_type();
                
                if($post_type == "post")
                    $links[] = array(
                        "is_home"   => false,
                        "url"       => get_permalink(get_option('page_for_posts')),
                        "label"     => get_the_title(get_option('page_for_posts'))
                    );

                // If it is a custom post type display name and link
                if($post_type != 'post') {

                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);

                    $links[] = array(
                        "is_home"   => false,
                        "url"       => $post_type_archive,
                        "label"     => $post_type_object->labels->name
                    );
                }

                // Get post category info
                $category = get_the_category();

                if(!empty($category)) {

                    // Get last category post is in
                    $last_category = end(array_values($category));

                    // Get parent any categories and create array
                    $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                    $cat_parents = explode(',',$get_cat_parents);

                    // Loop through parent categories and store in variable $cat_display
                    $cat_display = '';
                    foreach($cat_parents as $parents)
                        $links[] = array(
                            "is_home"   => false,
                            "url"       => "",
                            "label"     => $parents
                        );

                }

                // If it's a custom post type within a custom taxonomy
                $taxonomy_exists = taxonomy_exists($custom_taxonomy);
                if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                    $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                    $cat_id         = $taxonomy_terms[0]->term_id;
                    $cat_nicename   = $taxonomy_terms[0]->slug;
                    $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                    $cat_name       = $taxonomy_terms[0]->name;
                }

                // Check if the post is in a category
                if(!empty($last_category))
                    $links[] = array(
                        "is_home"   => false,
                        "url"       => "",
                        "label"     => get_the_title()
                    );
                // Else if post is in a custom taxonomy
                else if(!empty($cat_id)) {
                    $links[] = array(
                        "is_home"   => false,
                        "url"       => $cat_link,
                        "label"     => $cat_name
                    );
                    $links[] = array(
                        "is_home"   => false,
                        "url"       => "",
                        "label"     => get_the_title()
                    );
                }
                else
                    $links[] = array(
                        "is_home"   => false,
                        "url"       => "",
                        "label"     => get_the_title()
                    );

            }
            else if(is_category())
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => single_cat_title('', false)
                );
            else if(is_page()) {

                // Standard page
                if($post->post_parent) {

                    // If child page, get parents 
                    $anc = get_post_ancestors( $post->ID );

                    // Get parents in the right order
                    $anc = array_reverse($anc);

                    // Parent page loop
                    foreach($anc as $ancestor)
                        $links[] = array(
                            "is_home"   => false,
                            "url"       => get_permalink($ancestor),
                            "label"     => get_the_title($ancestor)
                        );

                    $links[] = array(
                        "is_home"   => false,
                        "url"       => "",
                        "label"     => get_the_title()
                    );
                } 
                else
                    $links[] = array(
                        "is_home"   => false,
                        "url"       => "",
                        "label"     => get_the_title()
                    );

            } 
            else if(is_tag()) {

                // Get tag information
                $term_id        = get_query_var('tag_id');
                $taxonomy       = 'post_tag';
                $args           = 'include=' . $term_id;
                $terms          = get_terms( $taxonomy, $args );
                $get_term_id    = $terms[0]->term_id;
                $get_term_slug  = $terms[0]->slug;
                $get_term_name  = $terms[0]->name;

                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => $get_term_name
                );
            } 
            else if(is_author()) {

                // Auhor archive

                // Get the author information
                global $author;
                $userdata = get_userdata($author);

                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => __("משתמשים", TL_LANGUAGE_SLUG)
                );
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => $userdata->display_name
                );
            } 
            else if (get_query_var('paged'))
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => get_query_var('paged')
                );
            else if (is_search())
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => __("תוצאות חיפוש עבור:", TL_LANGUAGE_SLUG) . " " . get_search_query()
                );
            else if(is_404())
                $links[] = array(
                    "is_home"   => false,
                    "url"       => "",
                    "label"     => "404"
                );
        }
        
        return $links;
    }
    
}