<?php

/**
    TL_Framework::$navbars array structure:
    "navigation location" => "navigation description" 
*/

TL_Framework::$navbars = array(
    "main-navigation" => "תפריט האתר הראשי",
    "footer1" => "תפריט חלק תחתון מס'1",
    "footer2" => "תפריט חלק תחתון מס'2",
    "footer3" => "תפריט חלק תחתון מס'3"
);