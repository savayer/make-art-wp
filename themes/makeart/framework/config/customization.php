<?php


TL_Customize::$config['header'] = array(
    "title"     => "חלק עליון",
    "priority"  => 1,
    "settings"  => array(
        "logo"  => array(
            "input"         => "picture",
            "label"         => "לוגו",
            "description"   => "בחר את לוגו האתר שלך"
        ),
        "cover"  => array(
            "input"         => "picture",
            "label"         => "תמונת קאבר",
            "description"   => "בחר את תמונת הקאבר עבור החלק העליון בעמוד הראשי"
        )
    )
);

TL_Customize::$config['contact'] = array(
    "title"     => "יצירת קשר",
    "priority"  => 2,
    "settings"  => array(
        "contact-phone"  => array(
            "input"         => "text",
            "label"         => "טלפון",
            "description"   => "הכנס טלפון ליצירת קשר עבור הלקוחות שלך"
        ),
        "contact-email"  => array(
            "input"         => "email",
            "label"         => "אימייל",
            "description"   => "הכנס אימייל ליצירת קשר עבור הלקוחות שלך"
        )
    )
);

TL_Customize::$config['social'] = array(
    "title"     => "רשתות חברתיות",
    "priority"  => 3,
    "settings"  => array(
        "facebook"  => array(
            "input"         => "url",
            "label"         => "פייסבוק",
            "description"   => "הכנס קישור לעמוד הפייסבוק של האתר"
        ),
        "instagram"  => array(
            "input"         => "url",
            "label"         => "אינסטגרם",
            "description"   => "הכנס קישור לפרופיל האינסטגרם של האתר"
        ),
        "google-plus"  => array(
            "input"         => "url",
            "label"         => "Google+",
            "description"   => "הכנס קישור לפרופיל האתר בגוגל פלוס "
        ),
        "youtube"  => array(
            "input"         => "url",
            "label"         => "YouTube",
            "description"   => "הכנס קישור לעמוד האתר ביוטיוב "
        )
    )
);