<?php

/**
    TL_Framework::$widgets array structure:

    "widget id" => array( // Unique widget id
        "name"          => "widget name", // Widget actual name
        "description"   => "widget description", // Short description
        "path"          => "widget.php", // Path to main widget file
        "classname"     => "Widget_Class", // Name of the main class which extends the TL_Widget_Base class
        "options"       => array(), // Extra widget options
        "control"       => array() // Extra control options,
    )
*/

if(current_theme_supports("poles"))
    TL_Framework::$widgets["poles"] = array(
        "name"          => "TL Poles",
        "description"   => "הכנס ווידגט של סקר",
        "path"          => "pole/pole.php",
        "classname"     => "TL_Pole_Widget"
    );


if(current_theme_supports("quotes"))
    TL_Framework::$widgets["quotes"] = array(
        "name"          => "TL Quotes",
        "description"   => "הכנס ווידגט של לקוחות ממליצים",
        "path"          => "quotes/quotes.php",
        "classname"     => "TL_Quotes_Widget"
    );


TL_Framework::$widgets["posts"] = array(
    "name"          => "TL Posts",
    "description"   => "הכנס ווידגט של פוסטים",
    "path"          => "posts/posts.php",
    "classname"     => "TL_Posts_Widget"
);