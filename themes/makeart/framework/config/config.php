<?php

/**
 * DEFAULT_THUMNBAIL
 * Default image for display when there is no thumbnail
 */
TL_Framework::$config['DEFAULT_THUMBNAIL'] = TL_CURRENT_THEME . "/assets/img/default-thumbnail.png";

/**
 * Instagram Helper
 */
TL_Framework::$config['INSTAGRAM'] = array(
    "client_id"             => "",
    "client_secret"         => "",
    "client_main_access"    => "",
    "redirect_url"          => get_site_url()
);

/**
 * Facebook 
 */
TL_Framework::$config['SOCIAL'] = array(
    "FACEBOOK_APP_ID" => "1793410554292368",
);