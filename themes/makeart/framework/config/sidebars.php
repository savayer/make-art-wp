<?php

/**
    TL_Framework::$sidebars array structure:

    "sidebar id" => array(
        "name" => "sidebar name",
        "description" => "sidebar description"
    )
*/

TL_Framework::$sidebars = array(
    
    "main-sidebar" => array(
        "name" => "סיידבאר עמודים פנימיים",
        "description" => "הסיידבאר הראשי באתר"
    )
);