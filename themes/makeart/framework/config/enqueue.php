<?php

TL_Framework::$enqueue = array(

    "product" => array(
        /**
         * JavaScript
         */
        array(
            "type" => "script",
            "id" => "FABRIC_JS",
            "src" => TL_CURRENT_THEME . "/assets/js/fabric.js",
        ),
        array(
            "type" => "script",
            "id" => "FABRIC_JS_CONTROLS",
            "src" => TL_CURRENT_THEME . "/assets/js/cutsomize-control.js",
        ),
        array(
            "type" => "script",
            "id" => "PRODUCT_JS",
            "src" => TL_CURRENT_THEME . "/assets/js/product.js",
        ),
        array(
            "type" => "style",
            "id" => "GOOGLE_FONTS",
            "src" => "//fonts.googleapis.com/css?family=Alef:700|Amatic+SC:700|Bellefair|M+PLUS+Rounded+1c|Miriam+Libre:700|Suez+One|Varela+Round&amp;subset=hebrew"
        )
    ),

    "archive" => array(
        /**
         * JavaScript
         */
        array(
            "type" => "script",
            "id" => "HISTORY_JS",
            "src" => TL_CURRENT_THEME . "/assets/js/history.js",
        ),
        array(
            "type" => "script",
            "id" => "ARCHIVE_JS",
            "src" => TL_CURRENT_THEME . "/assets/js/archive.js",
        )
    ),

    "general" => array(
        /**
         * CSS Stylesheets
         */ 
        array(
            "type" => "style",
            "id" => "BOOTSTRAP",
            "src" => TL_CURRENT_THEME . "/assets/css/bootstrap.css"
        ),
        array(
            "type" => "style",
            "id" => "MAIN_CSS",
            "src" => TL_CURRENT_THEME . "/assets/css/main.css"
        ),
        /**
         * JavaScript
         */
        array(
            "type" => "script",
            "id" => "BOOTSTRAP",
            "src" => "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js",
        ),
        array(
            "type" => "script",
            "id" => "JQUERY_STYLE_JS",
            "deps" => array("jquery"),
            "src" => TL_CURRENT_THEME . "/assets/js/jquery-style.js",
        ),
        array(
            "type" => "script",
            "id" => "MAIN_JS",
            "src" => TL_CURRENT_THEME . "/assets/js/main.js",
        )
    )
);