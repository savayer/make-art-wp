<?php
    
TL_Framework::$post_types['gallery'] = array(
    "labels" => array(
        "name" => "גלריית תמונות ועיצובים",
        "singular_name" => "תמונה"
    ),
    "public" => true,
    "show_ui" => true,
    "exclude_from_search" => true,
    "publicly_queryable" => false,
    "show_in_nav_menus" => false,
    "show_in_admin_bar" => false,
    "show_in_menu" => "#edit-content",
    "supports" => array('title','thumbnail')
);

TL_Framework::$post_types['video'] = array(
    "labels" => array(
        "name" => "סרטונים",
        "singular_name" => "סרטון"
    ),
    "public" => true,
    "show_ui" => true,
    "exclude_from_search" => true,
    "publicly_queryable" => false,
    "show_in_nav_menus" => false,
    "show_in_admin_bar" => false,
    "show_in_menu" => "#edit-content",
    "supports" => array('title')
);