<?php

abstract class TL_SampleBox extends TL_Metabox {
    
    /**
     * Array of post types to associate with the meta box
     * @var string[]
     */
    public static $post_types = array("post");
    
    /**
     * Name of the meta box
     * @var string
     */
    public static $name = "SAMPLE BOX";
    
    /**
     * Initialize fields settings.
     * 
     * Field key represent the meta_key which is associate with.
     * 
     * Field value represent the final value to save when post is saved.
     * @return void;
     */
    public static function metabox_fields() {
        
        self::$fields = array(
            "field1" => $_POST['field1'],
            "field2" => $_POST['field2']
        );
    }
}