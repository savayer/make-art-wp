<?php

/**
 * Abstract class to use as base to all metaboxes needed.
 * @author TL Interactive
 */
abstract class TL_Metabox {
    
    /**
     * Array of post types to associate with the meta box
     * @var string[]
     */
    public static $post_types = array();
    
    /**
     * Unique id for the metabox
     * @var string
     */
    public static $id = null;
    
    /**
     * Path of the metabox folder
     * @var string
     */
    public static $path;
    
    /**
     * Name of the meta box
     * @var string
     */
    public static $name = null;
    
    /**
     * File to include as the metabox body.
     * File location is relative to the main class folder.
     * @var string
     */
    public static $html = "metabox.php";
    
    /**
     * The context within the screen where the boxes should display.
     * 
     * Post edit screen contexts include 'normal', 'side', and 'advanced'.
     * @var string
     */
    public static $context = "normal";
    
    /**
     * The priority within the context where the boxes should show ("high", "low").
     * @default "default"
     * @var string 
     */
    public static $priority = "default";
    
    /**
     * Array of fields to save while post is saving and get while HTML is retrieved
     * @var mixed[]
     */
    public static $fields = array();
    
    /**
     * Array of data saved in the database
     * @var mixed[]
     */
    public static $data = array();
    
    /**
     * Initializing all metaboxes necessary hooks and filters
     * @param string $id Unique id for the metabox
     * @param string $path Path of the metabox folder
     * @return void
     */
    public static function init($id = "", $path = "") {
        
        static::$id = $id;
        
        static::$path = $path;
        
        static::metabox_fields();
        
        static::metabox_init();
        
        static::metabox_save();
    }
    
    /**
     * Initialize fields settings.
     * 
     * Field key represent the meta_key which is associate with.
     * 
     * Field value represent the final value to save when post is saved.
     * @return void;
     */
    abstract public static function metabox_fields();
    
    /**
     * Creates the actual metabox by calling "add_meta_boxes" action hook and add_meta_box method
     * @return void
     */
    public static function metabox_init() {
        
        add_action("add_meta_boxes", function(){
            
            add_meta_box(
                static::$id, 
                __(static::$name, TL_LANGUAGE_SLUG), 
                [static::class, 'metabox_content'],
                static::$post_types, 
                static::$context, 
                static::$priority
            );
        });
    }
    
    /**
     * Include the layout of the actual metabox
     * @param WP_Post $post Instance of the current edited post
     * @return void
     */
    public static function metabox_content($post) {
        
        $file = static::$path . "/" . static::$html;
        
        if(file_exists($file)) {
            
            foreach(static::$fields as $key => $value)
                static::$data[$key] = get_post_meta($post->ID, $key, true);
            
            require $file;
        }
    }
    
    /**
     * Takes care for all data storing after post is being saved
     * @return void
     */
    public static function metabox_save() {
        
        if(empty($_POST))
            return;
        
        add_action("save_post", function($post) {

            $post = get_post($post);
            
            if(in_array($post->post_type, static::$post_types)) {
                foreach(static::$fields as $key => $value)
                    update_post_meta($post->ID, $key, $value);
            }
        });
    }   
}