<?php


abstract class TL_Admin {
    
    /**
     * Contain a list of all the metaboxes and their settings
     * @var mixed[]
     */
    public static $metaboxes = array();
    
    /**
     * Contain a list of all extra menu pages
     * @var mixed[]
     */
    public static $menu = array();
    
    /**
     * Initializing necessary admin components
     * @return void
     */
    public static function init() {
        
        self::init_metaboxes();
        
        self::init_tables();
        
        self::init_menu();
        
        self::hide_index_editor();
    }
    
    /**
     * Initializing metaboxes config file and metaboxes classes
     * @return void
     */
    public static function init_metaboxes() {
        
        require dirname(__FILE__) . "/metabox.php";
        
        require dirname(__FILE__) . "/config/metaboxes.php";
        
        foreach(self::$metaboxes as $id => $metabox) {
            
            $classPath = dirname(__FILE__) . "/metaboxes/" . $metabox['location'];
            $className = $metabox['classname'];
            
            require $classPath;
            
            $className::init($id, dirname($classPath));
        }
    }
    
    /**
     * Initializing all table classes from tables folder
     * @return void
     */
    public static function init_tables() {
        
        global $pagenow;
        
        require dirname(__FILE__) . "/table.php";
        
        $files = scandir(dirname(__FILE__) . "/tables");
        
        foreach($files as $file) {
            
            if($file != "." && $file != "..") {
                
                require dirname(__FILE__) . "/tables/" . $file;

                $file_name_split = explode("-", str_replace(".php", "", $file));
                $classname = "TL_Table";
                foreach($file_name_split as $split)
                    $classname .= "_" . ucwords($split);
                
                $post_type = $_GET['post_type'];
                if($post_type == "")
                    $post_type = "post";
                
                if($pagenow == "edit.php" && $post_type == $classname::$post_type)
                    $classname::init();
            }
        }      
    }
    
    /**
     * Initializing the menu config file and loads the extra menu pages
     * @return void
     */
    public static function init_menu() {
        
        require dirname(__FILE__) . "/config/menu.php";
        
        add_action("admin_menu", function() {
            
            foreach(self::$menu as $slug => $menu) {
                
                if(!isset($menu['page_title']) || !isset($menu['menu_title'])) {
                    $menu['page_title'] = $menu['title'];
                    $menu['menu_title'] = $menu['title'];
                }
                
                add_menu_page(
                    __($menu['page_title'], TL_LANGUAGE_SLUG), 
                    __($menu['menu_title'], TL_LANGUAGE_SLUG), 
                    $menu['capability'], 
                    $slug,
                    $menu['callback'], 
                    $menu['icon'], 
                    $menu['position']
                );
            }
        });
    }
    
    /**
     * Hiding the editor from the index page
     * @global string $pagenow
     * @return void
     */
    public static function hide_index_editor() {
        
        global $pagenow;
        
        $index_id = get_option("page_on_front");
     
        if($pagenow != "post.php" || $_GET['post'] != $index_id)
            return;
        
        add_action("admin_init", function(){
            remove_post_type_support("page", "editor");
        });
    }
}