<?php

TL_Admin::$menu['#edit-content'] = array(
    "title" => "ניהול תוכן",
    "capability" => "manage_options",
    "icon" => "dashicons-edit",
    "position" => 30
);