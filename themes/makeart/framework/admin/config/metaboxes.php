<?php

/**
    TL_Admin::$metaboxes array structure:
    "metabox id" => array(
        "location"  => "", // Path to main metabox class
        "classname" => ""  // Name of the main metabox class
    )
*/

TL_Admin::$metaboxes = array(
    /*
    "samplebox" => array(
        "location"  => "samplebox/samplebox.php", // Path to main metabox class
        "classname" => "TL_SampleBox"  // Name of the main metabox class
    )
     */
);