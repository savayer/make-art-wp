<?php


abstract class TL_Table_Gallery extends TL_Table {
    
    /**
     * Which post type this table contains
     * @var string
     * @default null
     */
    public static $post_type = "gallery";
    
    /**
     * Determines if row actions should be display at all time
     * @var boolean
     * @default false
     */
    public static $always_show_actions = true;
    
    /**
     * Initializing necessary hooks and methods
     * @return void
     */
    public static function init() {
        
        self::$columns = array(
            "summary" => array(
                "label" => "תמונה",
                "data" => function($post_id) {
                    
                    $image = TL_Post::get_thumbnail_url($post_id);
                    $name = get_the_title($post_id);
                    
                    return '<img src="' . $image . '" style="width: 50px; height: 50px; object-fit: cover; float: right; margin-left: 10px;" alt="" />' . 
                           '<span style="font-weight: bold; font-size: 16px; margin-top: 2px; margin-bottom: 3px; display: block;">' . $name . '</span>';
                },
                "style" => "width: 300px;",
                "sortable" => true,
                "order_key" => "quote-name",
                "order_by" => "meta_value"
            )
        );
        
        parent::init();
    }
    
    /**
     * Enables table bulk actions
     * @var boolean
     * @default true
     */
    public static $enable_bulk_actions = false;
}