<?php


abstract class TL_Table_Post extends TL_Table {
    
    /**
     * Which post type this table contains
     * @var string
     * @default null
     */
    public static $post_type = "post";
    
    /**
     * Contain an array of columns settings
     * @var mixed[]
     * @default array()
     */
    /*
    public static $columns = array(
        "title",
        "field1" => array(
            "label"         => "מותאים אישית 1",
            "data"          => "field1",
            "sortable"      => true,
            "order_key"     => "field1",
            "order_by"      => "meta_value_num"
        ),
        "field2" => array(
            "label"         => "מותאם אישית 2",
            "data"          => "field2",
            "sortable"      => true,
            "order_key"     => "field2",
            "order_by"      => "meta_value_num"
        )
    );*/
    
    /**
     * Enables table bulk actions
     * @var boolean
     * @default true
     */
    public static $enable_bulk_actions = true;
}