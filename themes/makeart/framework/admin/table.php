<?php


abstract class TL_Table {
    
    /**
     * Which post type this table contains
     * @var string
     * @default null
     */
    public static $post_type = null;
    
    /**
     * Contain an array of columns settings
     * @var mixed[]
     * @default array()
     */
    public static $columns = array();
    
    /**
     * Enables table bulk actions
     * @var boolean
     * @default true
     */
    public static $enable_bulk_actions = true;
    
    /**
     * Determines if row actions should be display at all time
     * @var boolean
     * @default false
     */
    public static $always_show_actions = false;
    
    /**
     * Initializing necessary hooks and methods
     * @return void
     */
    public static function init() {
        
        static::init_columns();
    }
    
    /**
     * Initialize table columns
     * @return void
     */
    public static function init_columns() {
        
        if(empty(static::$columns))
            return;
        
        /**
         * Table columns header
         * manage_{$post_type}_posts_columns hook
         */
        
        add_filter("manage_" . static::$post_type . "_posts_columns", function($columns) {
            
            $oldColumns = $columns;
            $columns = array();
            
            if(static::$enable_bulk_actions)
                $columns['cb'] = "cb";
            
            foreach(static::$columns as $id => $column) {
                
                if(is_array($column))
                    $columns[$id] = __($column['label'], TL_LANGUAGE_SLUG);
                else if(is_string($id))
                    $columns[$id] = __($column, TL_LANGUAGE_SLUG);
                else if(isset($oldColumns[$column]))
                    $columns[$column] = $oldColumns[$column];
                else
                    $columns[$column] = $column;
            }
            
            return $columns;
            
        }, 400);
        
        /**
         * Table columns content
         * manage_{$post_type}_posts_custom_column hook
         */
        add_action("manage_" . static::$post_type . "_posts_custom_column", function($column, $post_id) {
            
            if(!isset(static::$columns[$column]))
                return;
            
            if(is_callable(static::$columns[$column]['data'])) {
                $function =& static::$columns[$column]['data'];
                echo $function($post_id);
            }
            else if(static::$columns[$column]['data'] != null) {
                
                $data = get_post_meta($post_id, static::$columns[$column]['data'], true);
                echo $data;
                
                if($data == null && isset(static::$columns[$column]['default']))
                    echo static::$columns[$column]['default'];
            }
            
        }, 400, 2);
        
        /**
         * Table sortable columns
         * manage_edit-{$post_type}_sortable_columns hook
         */
        add_action("manage_edit-" . static::$post_type . "_sortable_columns", function($columns) {
            
            foreach(static::$columns as $id => $column)
                if(is_array($column) && $column['sortable'])
                    $columns[$id] = $id;
            
            return $columns;
        });
        
        /**
         * Table sortable columns custom ordering
         * request hook
         */
        add_action("request", function($vars){
            
            global $pagenow;
            
            if($pagenow == "edit.php") {
                
                $post_type = $_GET['post_type'];
                if($post_type == null)
                    $post_type = "post";
                
                if($post_type == static::$post_type) {
                    
                    if(isset($vars['orderby']) && isset(static::$columns[$vars['orderby']])) {
                        
                        $column = static::$columns[$vars['orderby']];
                        
                        $vars['meta_key'] = $column['order_key'];
                        $vars['orderby'] = $column['order_by'];
                    }
                }
            }
            
            return $vars;
            
        });
        
        /**
         * Custom columns style
         * admin_head hook
         */
        add_action('admin_head', function(){
            
            $style = '<style type="text/css">';
            
            foreach(static::$columns as $id => $column)
                if(is_array($column) && isset($column['style']))
                    $style .= ".column-{$id}{" . $column['style'] . "}";
            
            if(static::$always_show_actions)
                $style .= "tr.type-" . static::$post_type . " .row-actions{position: static !important;}";
                    
            $style .= '</style>';
            
            echo $style;
        });
    }
}