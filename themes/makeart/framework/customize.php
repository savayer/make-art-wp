<?php


abstract class TL_Customize {
    
    /**
     * Contain an array of all sections and fields to be added to the Theme Customization Page
     * @var array[]
     */
    public static $config = array();
    
    /**
     * Contain a list of sections defined by the config file
     * @var array[]
     */
    public static $sections = array();
    
    /**
     * Contain a list of settings defined by the config file
     * @var array[]
     */
    public static $settings = array();
    
    /**
     * Holds an instance of the current WP_Customize_Manager instance
     * @var WP_Customize_Manager
     */
    public static $wp_customize = null;
    
    /**
     * Initiation of the config file and other components
     * @return void
     */
    public static function init() {
        
        require dirname(__FILE__) . "/config/customization.php";
        
        add_action('customize_register', function(WP_Customize_Manager $wp_customize) {
            
            self::$wp_customize =& $wp_customize;
            
            foreach(self::$config as $sectionID => $section) {
                
                // First, we'll register the section itself
                self::init_section($sectionID, $section);
                
                // Next step is registering the settings and make them work as a controls
                foreach($section['settings'] as $settingID => $setting) {
                    
                    self::init_setting($settingID, $setting);
                    
                    self::init_control($settingID, $setting, $sectionID);
                }
            }
        });
    }
    
    /**
     * Adds a section to the customization page
     * @param &string $sectionID Section unique id (slug)
     * @param &mixed[] $section Reference of the section
     * @return void
     */
    public static function init_section(&$sectionID, &$section) {
        
        $section['title'] = __($section['title'], TL_LANGUAGE_SLUG);

        if(isset($section['description']))
            $section['description'] = __($section['description'], TL_LANGUAGE_SLUG);
        
        self::$sections[] = $sectionID;

        self::$wp_customize->add_section($sectionID, $section);
    }
    
    /**
     * Prepare the setting so it can become an actual field (control)
     * @param &string $settingID Setting unique id (slug)
     * @param &mixed[] $setting Setting additional options
     * @return void
     */
    public static function init_setting(&$settingID, &$setting) {
        
        if(!isset($setting['default']))
            $setting['default'] = null;
        
        self::$wp_customize->add_setting($settingID, $setting);
    }
    
    /**
     * Creates the actual controller for a setting to be used
     * @param type $settingID The setting id to be associated with the control
     * @param type $setting Setting additional options
     * @param type $sectionID The section unique ID to be associated with the control
     * @return void
     */
    public static function init_control(&$settingID, &$setting, &$sectionID) {
        
        $control = array();
        
        $control['section'] = $sectionID;
        $control['settings'] = $settingID;
        $control['label'] = __($setting['label'], TL_LANGUAGE_SLUG);
        $control['description'] = __($setting['description'], TL_LANGUAGE_SLUG);
        
        switch($setting['input']) {
            
            case "picture": {
                self::$wp_customize->add_control(
                    new WP_Customize_Image_Control(
                        self::$wp_customize, 
                        $settingID, 
                        $control
                    )
                );
                break;
            }
            case "color": {
                self::$wp_customize->add_control(
                    new WP_Customize_Color_Control(
                        self::$wp_customize, 
                        $settingID, 
                        $control
                    )
                );
                break;
            }
            default: {
                self::$wp_customize->add_control($settingID, $control);
                break;
            }
        }
    }
}
