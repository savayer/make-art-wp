<?php

/**
 * Holds the domain which all language translation on the theme relate to
 * @var string
 */
define("TL_LANGUAGE_SLUG", "tl_framework");

/**
 * Holds the current theme directory URL, for assets and other URL needs
 * @var string
 */
define("TL_CURRENT_THEME", get_template_directory_uri());

/**
 * Site base URL
 * @var string
 */
define("TL_SITE_URL", get_site_url());

/**
 * Framework core class, initialize all necessary components
 *
 * This is an abstract class - no instances required nor allowed
 */
abstract class TL_Framework {

    /**
     * Contain a list of all the ajax calls
     * @var array[]
     */
    public static $ajax_calls = array();

    /**
     * Contain a list of all the custom post types and their settings
     * @var array[]
     */
    public static $post_types = array();

    /**
     * Contain a list of all the navigation bars
     * @var array[]
     */
    public static $navbars = array();

    /**
     * Contain a list of all the sidebars
     * @var array[]
     */
    public static $sidebars = array();

    /**
     * Contain a list of all the custom widgets
     * @var array[]
     */
    public static $widgets = array();

    /**
     * Contain a list of enqueue scripts and stylesheets
     * @var array[]
     */
    public static $enqueue = array();

    /**
     * Contain a list of supported theme features
     * @var mixed[]
     */
    public static $support = array();

    /**
     * General config info for helpers and other uses
     * @var mixed[]
     */
    public static $config = array();

    /**
     * Contain a list of localized Javascript objects
     * @var mixed[]
     */
    public static $localized_scripts = array();

    /**
     * Initializing all necessary components
     * @return void
     */
    public static function init() {

        // Starting a session for any component to use
        session_start();

        // Loading all components
        self::init_helpers_autoload();

        self::init_theme_support();

        self::init_ajax();

        self::init_post_types();

        self::init_navbars();

        self::init_sidebars();

        self::init_widgets();

        self::init_customization();

        self::init_localize_scripts();

        self::init_enqueue();

        self::init_admin();

        self::init_config();
    }

    /**
     * Initialize necessary config for AJAX to function
     * @return void
     */
    public static function init_ajax() {

        // Regitser necessary scripts
        add_action("wp_enqueue_scripts", function() {

            wp_localize_script("jquery", "TL_Ajax", array(
                "ajax_url" => admin_url("admin-ajax.php")
            ));

        });

    }

    /**
     * Setting up all stuff related to the custom post types
     * @return void
     */
    public static function init_post_types() {

        // Load the custom post types config file
        require dirname(__FILE__) . "/config/post-types.php";

        add_action("init", function(){

            foreach(self::$post_types as $slug => $args) {

                // Translation of labels and description of the post type
                if(isset($args['labels']) && is_array($args['labels']))
                    foreach($args['labels'] as $key => $value)
                        $args['labels'][$key] = __($value, TL_LANGUAGE_SLUG);

                if(isset($args['description']))
                    $args['description'] = __($args['description'], TL_LANGUAGE_SLUG);

                register_post_type($slug, $args);
            }
        });
    }

    /**
     * Setting up all stuff related to the navigation bars
     * @return void
     */
    public static function init_navbars() {

        // Load the navbars config file
        require dirname(__FILE__) . "/config/navbars.php";

        add_action("after_setup_theme", function() {

            foreach(self::$navbars as $location => $description)
                self::$navbars[$location] = __($description, TL_LANGUAGE_SLUG);

            register_nav_menus(self::$navbars);
        });
    }

    /**
     * Setting up all sidebars required for the theme
     * @return void
     */
    public static function init_sidebars() {

        // Load the sidebars config file
        require dirname(__FILE__) . "/config/sidebars.php";

        add_action("widgets_init", function(){

            foreach(self::$sidebars as $id => $sidebar) {

                $sidebar['name'] = __($sidebar['name'], TL_LANGUAGE_SLUG);
                $sidebar['description'] = __($sidebar['description'], TL_LANGUAGE_SLUG);

                $sidebar['id'] = $id;

                register_sidebar($sidebar);
            }
        });
    }

    /**
     * Setting up all the custom widgets of the theme
     * @return void
     */
    public static function init_widgets() {

        // Load the TL_Widget_Base class
        require dirname(__FILE__) . "/widget.php";

        // Load the widgets config file
        require dirname(__FILE__) . "/config/widgets.php";

        add_action("widgets_init", function() {

            foreach(self::$widgets as $id => $widget) {

                require dirname(__FILE__) . "/widgets/" . $widget['path'];

                $classname = $widget['classname'];

                // Define all widget settings on the TL_Widget_Base class
                $classname::$widget_id = $id;
                $classname::$widget_name = $widget['name'];
                $classname::$widget_description = $widget['description'];
                $classname::$widget_extra_options = $widget['options'];
                $classname::$widget_control_options = $widget['control'];

                register_widget($classname);
            }
        });
    }

    /**
     * Setting up all theme customization settings by initializing the TL_Customize
     * @return void
     */
    public static function init_customization() {

        require dirname(__FILE__) . "/customize.php";

        TL_Customize::init();
    }

    /**
     * Checks if page is admin page and if so initialize the TL_Admin class
     * @return void
     */
    public static function init_admin() {

        if(is_admin()) {

            require dirname(__FILE__) . "/admin/admin.php";

            TL_Admin::init();
        }
    }

    /**
     * Initializing an autoloader to make use of the helpers directory
     * @return void
     */
    public static function init_helpers_autoload() {

        require dirname(__FILE__) . "/helpers.php";

        TL_Helpers::init();
    }

    /**
     * Initializing support of theme features by calling add_theme_support
     * @return void
     */
    public static function init_theme_support() {

        require dirname(__FILE__) . "/config/support.php";

        foreach(self::$support as $feature => $args) {

            if(is_string($feature))
                add_theme_support($feature, $args);
            else
                add_theme_support($args);
        }

    }

    /**
     * Localizes a registered scripts with data for a JavaScript variable
     * @return void
     */
    public static function init_localize_scripts() {

        require dirname(__FILE__) . "/config/localized-scripts.php";

        add_action("wp_enqueue_scripts", function() {

            wp_localize_script("jquery", "TL_Config", self::$localized_scripts);
        });

    }

    /**
     * Initalize all scripts and stylesheets required for the current request
     * @return void
     */
    public static function init_enqueue($inRelation = null) {

        require dirname(__FILE__) . "/config/enqueue.php";

        add_action("wp_enqueue_scripts", function() {

            global $post, $enqueueRelation;

            $relatedEnqueue = array("general");

            if($enqueueRelation != null)
                $relatedEnqueue[] = $enqueueRelation;

            /**
             * Finding which scripts and styles are necessary for the current request.
             * Each relative enqueues would be added to $relatedEnqueue array
             */
            if(is_single() || is_page()) {

                $prefix = "post";
                if(is_page())
                    $prefix = "page";

                $relatedEnqueue[] = "{$prefix}-" . $post->ID;
                $relatedEnqueue[] = "{$prefix}-" . urldecode($post->post_name);
            }
            else if(is_front_page())
                $relatedEnqueue[] = "home";

            foreach($relatedEnqueue as $location) {

                if(isset(self::$enqueue[$location]) && is_array(self::$enqueue[$location])) {

                    foreach(self::$enqueue[$location] as $enqueue) {

                        if(!isset($enqueue['deps']))
                            $enqueue['deps'] = array();

                        if(!isset($enqueue['ver']))
                            $enqueue['ver'] = null;

                        if(!isset($enqueue['media']))
                            $enqueue['media'] = "all";

                        if($enqueue['type'] == "style")
                            wp_enqueue_style($enqueue['id'], $enqueue['src'], $enqueue['deps'], $enqueue['ver'], $enqueue['media']);
                        else
                            wp_enqueue_script($enqueue['id'], $enqueue['src'], $enqueue['deps'], $enqueue['ver'], true);
                    }
                }
            }
        });

    }

    /**
     * Loading the general config file
     * @return void
     */

    public static function init_config() {

        require dirname(__FILE__) . "/config/config.php";
    }
}