<?php

/**
 * Abstract class who only responsible for the autoloading of helpers.
 * 
 * All helpers are contained in the helpers directory at the framework base folder.
 */
abstract class TL_Helpers {
    
    /**
     * Initializing the autoloader for the helpers
     * @return void
     */
    public static function init() {
        
        spl_autoload_register(function($classname) {
            
            $file = dirname(__FILE__) . "/helpers/" . str_replace("tl_", "", strtolower($classname)) . ".php";
            
            if(file_exists($file))
                require $file;
        });
    }
    
}