<?php


class TL_Posts_Widget extends TL_Widget_Base {
    
    public static $post_id = null;
    
    /**
     * Echos the actual widget layout on the front-end.
     * @param type $args
     * @param type $instance
     * @return void
     */
    function widget($args, $instance) {
        
        $args = array(
            "post_type" => "post",
            "posts_per_page" => $instance['limit']
        );
        
        if($instance['hide_current'] && is_single()) 
            $args['post__not_in'] = array(self::$post_id);
        
        if($instance['type'] == "last") {
            $args['order'] = "DESC";
            $args['orderby'] = "date";
        }
        else if($instance['type'] == "most_viewed") {
            $args['order'] = "DESC";
            $args['orderby'] = "meta_value_num";
            $args['meta_key'] = "views";
        }
        
        $posts = new WP_Query($args);
        
        require(locate_template("templates/widgets/widget-posts.php"));
    }

    /**
     * Update widget data when form is saved
     * @param type $new_instance
     * @param type $old_instance
     * @return array[] Final instance to save on database
     */
    function update($new_instance, $old_instance) {
        
        $instance = array();
        
        $instance['title'] = $new_instance['title'];
        $instance['type'] = $new_instance['type'];
        
        if($new_instance['limit'] != "")
            $instance['limit'] = (int)$new_instance['limit'];
        else
            $instance['limit'] = 5;
        
        $instance['hide_current'] = $new_instance['hide_current'];
        
        return $instance;
    }
    

    /**
     * Echos the layout of the form in the back-end.
     * @param type $instance
     * @return void
     */
    function form($instance) {
        
        require dirname(__FILE__) . "/form.php";
    }
}

add_action("template_redirect", function() {
    
    TL_Posts_Widget::$post_id = get_the_ID();
});