<p>
    <label for="<?=esc_attr($this->get_field_id("title"));?>"><b>כותרת:</b></label>
    <input type="text" style="width: 100%;" name="<?=esc_attr($this->get_field_name("title"));?>" value="<?=$instance['title'];?>" id="<?=esc_attr($this->get_field_id("title"));?>" />
</p>
<p>
    <label for="<?=esc_attr($this->get_field_id("type"));?>"><b>הצג פוסטים:</b></label>
    <select type="text" id="<?=esc_attr($this->get_field_id("type"));?>" name="<?=esc_attr($this->get_field_name("type"));?>" style="width: 100%;">
        <option value="last">אחרונים</option>
        <option value="most_viewed" <?=selected($instance["type"] == "most_viewed");?>>הנצפים ביותר</option>
    </select>
</p>
<p>
    <label for="<?=esc_attr($this->get_field_id("limit"));?>"><b>מגבלת פוסטים לשליפה:</b></label>
    <input type="text" style="width: 100%;" name="<?=esc_attr($this->get_field_name("limit"));?>" value="<?=$instance['limit'];?>" placeholder="ברירת המחדל היא 5" id="<?=esc_attr($this->get_field_id("limit"));?>" />
</p>

<p>
    <input type="checkbox" value="1" <?= checked($instance['hide_current'] == 1);?> name="<?=esc_attr($this->get_field_name("hide_current"));?>" id="<?=esc_attr($this->get_field_id("hide_current"));?>" />
    <label for="<?=esc_attr($this->get_field_id("hide_current"));?>"><b>אל תציג פוסט נוכחי בעמוד פוסט</b></label>
</p>