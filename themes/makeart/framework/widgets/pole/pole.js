(function($) {
    
    $(document).ready(function() {
        
        $(".widget.pole form.voted .vote-rate-full").each(function() {
            $(this).css("width", $(this).data("rate") + "%");
        });
        
        $(".widget.pole form.voted .vote-number").each(function(){
            $(this).prop('Counter', 0).animate({
                Counter: Number($(this).data("rate"))
            }, {
                duration: 1500,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        
        $(".widget.pole form").submit(function(e){
           
            e.preventDefault();
            
            var form = $(this);
            
            form.find(".button[type=submit], input[type=submit]").prop("disabled", "disabled");
            
            $.ajax({
                url: TL_Ajax.ajax_url + "?action=pole_vote",
                data: form.serialize(),
                type: "POST",
                success: function(result) {
                    
                    result = $.parseJSON(result);
                    
                    form.addClass("voted");
                    
                    setTimeout(function(){

                        $.each(result, function(key, value){

                            form.find(".vote-rate-" + key).css("width", value + "%");
                        
                            form.find(".vote-rate-number-" + key).prop('Counter', 0).animate({
                                Counter: Number(value)
                            }, {
                                duration: 1500,
                                easing: 'swing',
                                step: function (now) {
                                    $(this).text(Math.ceil(now));
                                }
                            });
                        });
                    }, 200);
                    console.log(result);
                }
            });
        });
        
    });
    
})(jQuery);