<?php


class TL_Pole_Widget extends TL_Widget_Base {
    
    /**
     * Holds all poles for the widget form select input
     * @var WP_Post[] Array of WP_Post object
     */
    public static $poles = array();
    
    /**
     * Holds pole data for the current widget
     * @var mixed[]
     */
    public $pole = array();
    
    /**
     * Echos the actual widget layout on the front-end.
     * @param type $args
     * @param type $instance
     * @return void
     */
    function widget($args, $instance) {
        
        $pole = get_post($instance['pole_id']);
        
        $this->pole = array(
            "pole_id" => (int)$instance['pole_id'],
            "question" => $pole->post_title,
            "answers" => array(),
            "votes_rate" => self::get_votes_rate((int)$instance['pole_id']),
            "voted" => false
        );
        
        if(isset($_COOKIE["vfp_" . $this->pole['pole_id']]))
            $this->pole['voted'] = true;
        
        while(have_rows("answers", (int)$instance['pole_id'])) {
            
            the_row();
            
            $this->pole['answers'][] = get_sub_field("answer");
        }
        
        wp_enqueue_script("widget-pole-js", TL_CURRENT_THEME . "/framework/widgets/pole/pole.js", array("jquery"));
        
        require(locate_template("templates/widgets/widget-pole.php"));
        
    }

    /**
     * Update widget data when form is saved
     * @param type $new_instance
     * @param type $old_instance
     * @return array[] Final instance to save on database
     */
    function update($new_instance, $old_instance) {
        
        $instance = array();
        
        $instance['pole_id'] = (int)$new_instance['pole_id'];
        
        return $instance;
    }
    

    /**
     * Echos the layout of the form in the back-end.
     * @param type $instance
     * @return void
     */
    function form($instance) {
        
        self::$poles = get_posts(array(
            "post_type" => "pole",
            "posts_per_page" => -1
        ));
        
        require dirname(__FILE__) . "/form.php";
        
    }
    
    /**
     * An AJAX call performing the actual vote
     * @return void
     */
    public static function ajax_vote() {
        
        $pole_id = (int)$_POST['pole_id'];
        
        if(!isset($_COOKIE['vfp_' . $pole_id])) {
            
            $totalVotes = (int)get_post_meta($pole_id, "total_votes", true);
            $totalVotes++;
            
            $answer = (int)$_POST['pole-answer'];
            $totalAnswerVotes = (int)get_post_meta($pole_id, "total_votes_" .  $answer, true);
            $totalAnswerVotes++;
            
            update_post_meta($pole_id, "total_votes", $totalVotes);
            update_post_meta($pole_id, "total_votes_" .  $answer, $totalAnswerVotes);
            
            setcookie("vfp_" . $pole_id, $answer, time() + (3600 * 24 * 7 * 4 * 12 * 3), "/");
        }
            
        $votes_rate = self::get_votes_rate($pole_id);
        
        echo json_encode($votes_rate);
        
        die();
    }
    
    /**
     * Return the votes rate in percentage by the pole id
     * @param type $pole_id Pole uniuqe id
     * @return int[] Array of full numbers
     */
    public static function get_votes_rate($pole_id) {
        
        $votes_rate = array();
        $total_votes = (int)get_post_meta($pole_id, "total_votes", true);
        
        while(have_rows("answers", $pole_id)) {
            
            the_row();
            
            $answer_votes = (int)get_post_meta($pole_id, "total_votes_" . sizeof($votes_rate), true);
            
            if($total_votes == 0 || $answer_votes == 0)
                $votes_rate[] = 0;
            else
                $votes_rate[] = (int)round(($answer_votes / $total_votes) * 100); 
        }
        
        return $votes_rate;
    }
}

add_action("wp_ajax_pole_vote", "TL_Pole_Widget::ajax_vote");
add_action("wp_ajax_nopriv_pole_vote", "TL_Pole_Widget::ajax_vote");