<p>
    <label for="<?=esc_attr($this->get_field_id("pole_id"));?>"><b>בחר סקר לשיוך:</b></label>
    <select style="width: 100%;" id="<?=esc_attr($this->get_field_id("pole_id"));?>" name="<?=esc_attr($this->get_field_name("pole_id"));?>">
        <option value=""></option>
        <?
            foreach(self::$poles as $pole) {
        ?>
        <option value="<?=$pole->ID;?>" <?=selected($pole->ID == $instance['pole_id']);?>><?=$pole->post_title;?></option>
        <?
            }
        ?>
    </select>
</p>