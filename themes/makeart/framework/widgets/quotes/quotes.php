<?php


class TL_Quotes_Widget extends TL_Widget_Base {
    
    /**
     * Echos the actual widget layout on the front-end.
     * @param type $args
     * @param type $instance
     * @return void
     */
    function widget($args, $instance) {
        
        $quotes = new WP_Query(array(
            "post_type" => "quote",
            "posts_per_page" => -1
        ));
        
        require(locate_template("templates/widgets/widget-quotes.php"));
    }

    /**
     * Update widget data when form is saved
     * @param type $new_instance
     * @param type $old_instance
     * @return array[] Final instance to save on database
     */
    function update($new_instance, $old_instance) {
        
        $instance = array();
        
        $instance['title'] = $new_instance['title'];
        $instance['arrows'] = (int)$new_instance['arrows'];
        
        return $instance;
    }
    

    /**
     * Echos the layout of the form in the back-end.
     * @param type $instance
     * @return void
     */
    function form($instance) {
        
        require dirname(__FILE__) . "/form.php";
    }
}