<p>
    <label for="<?=esc_attr($this->get_field_id("title"));?>"><b>כותרת:</b></label>
    <input type="text" style="width: 100%;" name="<?=esc_attr($this->get_field_name("title"));?>" value="<?=$instance['title'];?>" id="<?=esc_attr($this->get_field_id("title"));?>" />
</p>

<p>
    <label for="<?=esc_attr($this->get_field_id("arrows"));?>"><b>הצג חיצים:</b></label><br />
    <select style="width: 100%;" name="<?=esc_attr($this->get_field_name("arrows"));?>" id="<?=esc_attr($this->get_field_id("arrows"));?>">
        <option value="0">לא</option>
        <option value="1" <?=selected($instance['arrows'] == 1);?>>כן</option>
    </select>
</p>