<?php


/**
 * Abstract class to use as a base for all widgets on the framework
 * 
 * Extends the WP_Widget default class
 * 
 * Offers better control over widgets
 */

abstract class TL_Widget_Base extends WP_Widget {
    
    /**
     * Unique identifier for the a specific widget
     * @var string
     */
    public static $widget_id;
    
    /**
     * Widget name as it shows on the admin panel
     * @var string
     */
    public static $widget_name;
    
    /**
     * Short description about the widget
     * @var string
     */
    public static $widget_description;
    
    /**
     * Extra widget options can be found at the link:
     * 
     * https://codex.wordpress.org/Widgets_API
     * @var string
     */
    public static $widget_extra_options = array();
    
    /**
     * Extra control options can found at the link:
     * 
     * https://codex.wordpress.org/Widgets_API
     * @var string
     */
    public static $widget_control_options = array();
    
    
    /**
     * Alternative constructor extending from WP_Widget class
     * @return void
     */
    public function __construct() {
        
        // Translate widget name and description
        self::$widget_extra_options["description"] = __(self::$widget_description, WP_LANGUAGE_SLUG . "_widgets");
        
        self::$widget_name = __(self::$widget_name, WP_LANGUAGE_SLUG . "_widgets");
        
        // Call to the original constructor from WP_Widget class
        parent::__construct(
            self::$widget_id, 
            self::$widget_name, 
            self::$widget_extra_options,
            self::$widget_control_options
        );
    }
}