<?php

    error_reporting(E_ERROR);

	require "framework/core.php";
	
	require "classes/classes.php";
    
    TL_Framework::init();

    acf_add_options_page(array(
		'page_title' 	=> 'הגדרות נוספות',
		'menu_title' 	=> 'הגדרות נוספות',
		'menu_slug' 	=> 'extra-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

	/**
	 * Disable billing input auto focus
	 */
	add_filter('woocommerce_checkout_fields', function($fields){
		$fields['billing']['billing_first_name']['autofocus'] = false;
		return $fields;
	});

	add_filter('acf/settings/show_admin', 'my_acf_show_admin');

	function my_acf_show_admin($show) {
		return current_user_can('edit_plugins');
	}
	
	if(isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] != 1 && !$_GET['s'])
		add_filter('loop_shop_per_page', create_function( '$cols', 'return ' . (16*(int)$_GET['page']) . ';'), 20);
	else if(isset($_GET['s']))
		add_filter('loop_shop_per_page', create_function( '$cols', 'return -1;'), 20);

	add_filter( 'woocommerce_order_item_name', function($item_name, $item) {
		$_product = get_product( $item['variation_id'] ? $item['variation_id'] : $item['product_id'] );
		$link = get_permalink( $_product->id );
		return '<a href="'. $link .'"  rel="nofollow">'. $item_name .'</a>';
	}, 10, 2);

    add_filter( 'request', 'my_request_filter' );
    function my_request_filter( $query_vars ) {
        if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
            $query_vars['s'] = " ";
        }
        return $query_vars;
    }