<?php

/**
 * TL Framework theme template
 * @author TL Interactive
 */
?>
        <footer id="footer">

            <div class="container">

                <a href="#" class="logo">
                    <img src="<?=TL_CURRENT_THEME;?>/assets/img/logo.png" alt="" />
                </a>

                <div class="row">

                    <div class="col-md-3 address-wrapper">
                        <?=get_field("footer-text", "option");?>
                    </div>

                    <div class="col-md-3 footer-links">
                        
                        <?
                            $menuID = TL_Nav::get_menu("footer1");
                            $menuItems = wp_get_nav_menu_items($menuID);
                            $menuObject = wp_get_nav_menu_object($menuID);
                        ?>

                        <h6 class="title"><?=$menuObject->name;?></h6>

                        <ul>
                            <?
                                foreach($menuItems as $item) {
                            ?>
                            <li>
                                <a href="<?=$item->url;?>"> 
                                    <?=TL_Nav::get_title($item);?>
                                </a>
                            </li>
                            <?
                                }
                            ?>
                        </ul>

                    </div>
                    
                    <div class="col-md-3 footer-links">
                        
                        <?
                            $menuID = TL_Nav::get_menu("footer2");
                            $menuItems = wp_get_nav_menu_items($menuID);
                            $menuObject = wp_get_nav_menu_object($menuID);
                        ?>

                        <h6 class="title"><?=$menuObject->name;?></h6>

                        <ul>
                            <?
                                foreach($menuItems as $item) {
                            ?>
                            <li>
                                <a href="<?=$item->url;?>"> 
                                    <?=TL_Nav::get_title($item);?>
                                </a>
                            </li>
                            <?
                                }
                            ?>
                        </ul>

                    </div>
                    
                    <div class="col-md-3 footer-links">

                        <?
                            $menuID = TL_Nav::get_menu("footer3");
                            $menuItems = wp_get_nav_menu_items($menuID);
                            $menuObject = wp_get_nav_menu_object($menuID);
                        ?>

                        <h6 class="title"><?=$menuObject->name;?></h6>

                        <ul>
                            <?
                                foreach($menuItems as $item) {
                            ?>
                            <li>
                                <a href="<?=$item->url;?>"> 
                                    <?=TL_Nav::get_title($item);?>
                                </a>
                            </li>
                            <?
                                }
                            ?>
                        </ul>

                    </div>

                </div>

                <div class="secure-payment row d-flex justify-content-between">

                    <p class="info align-self-center">
                    <img src="<?=TL_CURRENT_THEME;?>/assets/img/secure-payment.png" alt="" />
                    <?=get_field("footer-secure", "option");?>
                    </p>

                    <div class="payment-methods align-self-center">
                        <img src="<?=TL_CURRENT_THEME;?>/assets/img/payments.png" alt="" />
                    </div>

                </div>

            </div>

            <div class="credits"><?=get_field("footer-copyrights", "option");?></div>

        </footer>

    </div>
    
    <div id="video-wrapper">
        <div class="video-inner">
            <a href="javascript:;" class="close-video">&times;</a>
            <iframe src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>

    <?
        wp_footer();
    ?>

</body>
</html>