<?php

/**
 * TL Framework theme template
 * @author TL Interactive
 */

the_post();

global $enqueueRelation;

$enqueueRelation = "product";

new Product();

if(isset($_POST['p-id'])) {
    Cart::add_to_cart();
    die();
} 

get_header();

$disableImageUpload = (bool)get_post_meta(get_the_ID(), "disable-image-upload", true);
$disableCustomText = (bool)get_post_meta(get_the_ID(), "disable-custom-text", true);
$disableStickerPicker = (bool)get_post_meta(get_the_ID(), "disable-sticker-picker", true);

?>
<section id="product" class="layer-0 <?=($disableCustomText && $disableImageUpload && $disableStickerPicker) ? ("edit-disabled") : ("");?>" data-color="<?=get_post_meta(get_the_ID(), "image-color", true);?>">

    <div class="container">

        <div class="row">
            <h1 class="section-title">
                <span><?= get_the_title(); ?></span>
            </h1>

            <div class="row product-row">

                <div class="col-md-5">

                    <div class="gallery">

                        <div class="block-title">
                            <h3 class="product-name"><?= get_the_title(); ?></h3>
                            <span class="manufactor">יצרן: LaerArt</span>
                            <span>מקט: <?=Product::$product->get_sku();?></span>
                        </div>

                      



  <div class="large-image">

                            <img src="<?= Product::$CROP_IMAGES[0]['url']; ?>" alt="" />

                            <div class="canvas-wrapper">
                                <?
                                foreach (Product::$CROP_IMAGES as $index => $image) {

                                    $originalWidth = $image['crop']['width'];
                                    $originalHeight = $image['crop']['height'];

                                    $croppedX = (100 / $originalWidth) * $image['crop']['data']['x'];
                                    $croppedY = (100 / $originalHeight) * $image['crop']['data']['y'];
                                    $croppedWidth = (100 / $originalWidth) * $image['crop']['data']['width'];
                                    $croppedHeight = (100 / $originalHeight) * $image['crop']['data']['height'];
                                    ?>
                                <div class="canvas-layer layer-<?=$index;?>-related layer-related" data-image="<?=base64_encode($image['url']);?>" data-index="<?=$index;?>" data-width="<?=$croppedWidth;?>" data-height="<?=$croppedHeight;?>" data-x="<?=$croppedX;?>" data-y="<?=$croppedY;?>">
                                    <canvas id="canvas-layer-<?=$index;?>"></canvas>
                                </div>
                                <?
                            }
                            ?>
                            </div>
                        </div>






                        <?
                        if (sizeof(Product::$CROP_IMAGES) > 1) {
                            ?>
                        <div class="row gallery-images">
                        <?
                        foreach (Product::$CROP_IMAGES as $index => $image) {
                            ?>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="image" data-index="<?=$index;?>">
                                    <img src="<?= $image['url']; ?>" alt="" />
                                </div>
                            </div>
                            <?
                        }
                        ?>
                        </div>
                        <?
                    }
                    ?>

                    </div>

                    <?
                        if(get_the_content() != "") {
                    ?>
                    <div class="description desktop-description">

                        <h6 class="block-title">תיאור המוצר</h6>

                        <div class="content">
                            <?
                            the_content();
                            ?>
                        </div>

                    </div>
                    <?
                        }
                    ?>

                </div>

                <div class="col-md-7">

                    <form id="product-form" action="" method="POST" enctype="multipart/form-data">

                        <input type="hidden" name="p-id" value="<?=Product::$id;?>" />

                        <?
                        foreach (Product::$CROP_IMAGES as $index => $image) {
                        ?>

                        <input type="hidden" id="preview-<?=$index;?>" name="preview-<?=$index;?>" value="" />

                        <div class="layer-<?= $index; ?>-related layer-related">
                                
                            <?
                                $stepsCount = 0;
                                if(!$disableImageUpload) {
                            ?>
                            <div class="image-upload empty order-step" data-index="<?=$index;?>">

                                <h5 class="block-title step" data-step="<?=++$stepsCount;?>">
                                    העלה תמונה
                                </h5>

                                <div class="step-box-content">

                                    <div class="settings">

                                        <p class="info">
                                            * עם העלאת התמונה אני מאשר שזכויות התמונה שייכות לי
                                        </p>

                                        <div class="inputs">

                                            <div class="stauration-container">
                                                <button type="button" class="control minus">-</button>
                                                <input type="range" min="0" step="1" max="255" value="128" class="threshold slider" />
                                                <button type="button" class="control plus">+</button>
                                                <input type="number" min="0" max="255" value="128" />
                                            </div>

                                            <button type="button" class="invert-button button simple blue">היפוך סיטורציה</button>
                                            
                                            <div class="file">
                                                <input type="file" name="file-<?=$index;?>" accept=".jpeg,.jpg,.png,.ico" />
                                                <span class="button simple blue">צרף קובץ</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="preview">
                                        <img src="" alt="" />
                                    </div>
                                </div>

                            </div>
                            <?
                                }
                                if(!$disableCustomText) {
                            ?>
                            <div class="custom-text order-step" data-index="<?=$index;?>">

                                <h5 class="block-title step" data-step="2">
                                    הכנס טקסט
                                </h5>

                                <div class="step-box-content">

                                    <label>
                                        הטקסט שלך כאן:
                                    </label>

                                    <div class="text-slot text-<?=$index;?>-0">
                                        <a href="javascript:;" class="open-font-picker">
                                            <img src="<?=TL_CURRENT_THEME;?>/assets/img/icons/font.png" alt="" />
                                            בחר פונט
                                        </a>
                                        <textarea data-index="0" name="text-<?=$index;?>[]"></textarea>
                                    </div>
                                    <button type="button" class="button simple blue">טקסט נוסף</button>
                                </div>

                                <ul class="font-picker">
                                    <li style="font-family: 'Alef';" class="active"><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'Amatic SC';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'Bellefair';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'M PLUS Rounded 1c';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'Suez One';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'Varela Round';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'Miriam Libre';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                    <li style="font-family: 'GveretLevin';" class=""><a href="javascript:;">פונט לדוגמא</a></li>
                                </ul>
                            </div>
                            <?
                                }
                                if(!$disableStickerPicker) {
                            ?>
                            <div class="sticker-picker order-step">
                            
                                <h5 class="block-title step" data-step="3">
                                    בחר אייקון
                                </h5>

                                <div class="step-box-content">
                                    <div id="stickers-carousel" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <ul class="stickers-row">
                                                    <?
                                                        $count = 0;
                                                        while(have_rows("icons", "options")) {
                                                            the_row();
                                                            $image = get_sub_field("icon");
                                                    ?>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <img src="<?=$image['url'];?>" alt="" />
                                                        </a> 
                                                    </li>
                                                    <?
                                                            $count++;
                                                            if($count == 8) {
                                                                echo '</ul></div><div class="carousel-item"><ul class="stickers-row">';
                                                                $count = 0;
                                                            }
                                                        }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <?
                                            if(count(get_field("icons", "option")) > 8) {
                                        ?>
                                        <a class="carousel-control-prev" href="#stickers-carousel" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#stickers-carousel" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                        <?
                                            } 
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?
                                }
                            ?>

                        </div>
                        <?
                        }
                        ?>
                        <?
                            if(get_the_content() != "") {
                        ?>
                        <div class="description mobile-description">

                            <h6 class="block-title">תיאור המוצר</h6>

                            <div class="content">
                                <?
                                the_content();
                                ?>
                            </div>

                        </div>
                        <?
                            }
                        ?>
                        <div class="summary">

                            <div class="price" id="price" data-base="<?=Product::$product->get_price();?>" data-image="<?=Product::$image_price;?>">
                                סה"כ <span>₪<?=number_format(Product::$product->get_price(), 2, ".", "");?></span>
                                <?php if (Product::$product->get_regular_price() != Product::$product->get_price()):
                                ?>
                                <span class="regular-price">₪<?=number_format(Product::$product->get_regular_price(), 2, ".", "");?></span>
                                <?php endif;?>
                            </div>

                            <button type="submit" class="button simple blue bold">הוסף לעגלת הקניות</button>
                            
                        </div>

                    </form>

                </div>

            </div>
        </div>

    </div>

</section>
<?php
get_footer();
?>