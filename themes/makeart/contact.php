<?php
    /**
     * Template Name: יצירת קשר
     * TL Framework theme template
     * @author TL Interactive
     */

    the_post();

    get_header();

?>
<div id="contact" class="single">
    <?
        the_content();
    ?>
</div>
<?
    get_footer();